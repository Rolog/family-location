# О проекте Family Location

Проект предусматривает возможность определения местоположения членов семьи и удобного отображения этого местоположения в приложении.

Данное **android**-приложение работает как клиентская часть в связке с серверной частью [Family Location Server](https://bitbucket.org/Rolog/family-location-server).

# Установка

Для установки **android**-приложения **Family Location** необходимо скачать установочный файл [FamilyLocation.apk](https://bitbucket.org/Rolog/family-location/downloads/FamilyLocation.apk) из раздела ["Загрузки"](https://bitbucket.org/Rolog/family-location/downloads/)  и установить на своем **android**-устройстве.

# Инструкции

* [Разрешения](https://bitbucket.org/Rolog/family-location/wiki/%D0%A0%D0%B0%D0%B7%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F)
* [Регистрация/Вход](https://bitbucket.org/Rolog/family-location/wiki/%D0%A0%D0%B5%D0%B3%D0%B8%D1%81%D1%82%D1%80%D0%B0%D1%86%D0%B8%D1%8F)
* [Меню](https://bitbucket.org/Rolog/family-location/wiki/%D0%9C%D0%B5%D0%BD%D1%8E)
* [Просмотр местоположения](https://bitbucket.org/Rolog/family-location/wiki/%D0%9F%D1%80%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80%20%D0%BC%D0%B5%D1%81%D1%82%D0%BE%D0%BF%D0%BE%D0%BB%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F)
* [Аккаунт](https://bitbucket.org/Rolog/family-location/wiki/%D0%90%D0%BA%D0%BA%D0%B0%D1%83%D0%BD%D1%82)
* [Семья](https://bitbucket.org/Rolog/family-location/wiki/%D0%A1%D0%B5%D0%BC%D1%8C%D1%8F)
* [Родственники](https://bitbucket.org/Rolog/family-location/wiki/%D0%A0%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D0%B8%D0%BA%D0%B8)
* [Локации](https://bitbucket.org/Rolog/family-location/wiki/%D0%9B%D0%BE%D0%BA%D0%B0%D1%86%D0%B8%D0%B8)
* [Запросы мне](https://bitbucket.org/Rolog/family-location/wiki/%D0%97%D0%B0%D0%BF%D1%80%D0%BE%D1%81%D1%8B%20%D0%BC%D0%BD%D0%B5)
* [Настройки](https://bitbucket.org/Rolog/family-location/wiki/%D0%9D%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B8)