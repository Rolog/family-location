package com.morozov.familylocation.AsyncTask;


import java.net.URL;

public class TaskObject {
    private TaskType protocol;
    private TaskType type;
    private URL url;
    private String body;

    public TaskObject(TaskType protocol, TaskType type, URL url, String body) {
        this.protocol = protocol;
        this.type = type;
        this.url = url;
        this.body = body;
    }

    public TaskType getProtocol() {
        return protocol;
    }

    public void setProtocol(TaskType protocol) {
        this.protocol = protocol;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
