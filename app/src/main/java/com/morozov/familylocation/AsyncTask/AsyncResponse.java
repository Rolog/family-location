package com.morozov.familylocation.AsyncTask;

public interface AsyncResponse {
    void processFinish(TaskObject output);
}
