package com.morozov.familylocation.AsyncTask;

import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.PathOnMapActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.Server.MDBAccount;
import com.morozov.familylocation.Server.MDBPosition;
import com.morozov.familylocation.Server.MDBRequestme;
import com.morozov.familylocation.data.model.LoggedInUser;
import com.morozov.familylocation.enteties.CurrentAccount;
import com.morozov.familylocation.enteties.Position;
import com.morozov.familylocation.enteties.RelativeInLocation;
import com.morozov.familylocation.enteties.Requestme;
import com.morozov.familylocation.ui.location.LocationFragment;

import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class RestRequestsService {
    public String getBaseUrl() {
        return  (MainActivity.settings.getProtocol() != null ? MainActivity.settings.getProtocol() : "") + "://" +
                MainActivity.settings.getUrl() +
                (MainActivity.settings.getPort() != null ? ":" + MainActivity.settings.getPort(): "");
    }

    public Boolean testConnect() {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
            }
        });
        try {
            TaskObject result = callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/test_connect"), null)).get();
            if (result != null && result.getBody() != null && result.getBody().equals("Ok"))
                return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void updateAccount() {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                if (callbackResponse != null && callbackResponse.getBody() != null) {
                    MDBAccount account = JSONtoClass(callbackResponse.getBody(), MDBAccount.class);
                    if (account != null && account.getId() != null) {
                        if (account.getModified() > MainActivity.currentAccount.getModified()) {
                            MainActivity.dataBaseEntityService.saveAccount(new CurrentAccount(account));
                            MainActivity.dataBaseEntityService.loadCurrentAccount();
                        } else if (account.getModified() < MainActivity.currentAccount.getModified()) {
                            exportAccountToServer();
                        }
                    } else {
                        exportAccountToServer();
                    }
                }
                 else
                    Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.no_connect), Toast.LENGTH_SHORT).show();
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/account/" + MainActivity.currentAccount.getIdUUID()), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void exportAccountToServer() {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                updateRequestsme();
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.POST,
                    new URL(getBaseUrl() + "/rest/account"), classToJSON(new MDBAccount(MainActivity.currentAccount))));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void checkUpdateAccount() {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                if (callbackResponse != null && callbackResponse.getBody() != null) {
                    if (Boolean.parseBoolean(callbackResponse.getBody())) {
                        updateAccount();
                    }
                } else
                    Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.no_connect), Toast.LENGTH_SHORT).show();
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/account_check_update?id=" + MainActivity.currentAccount.getIdUUID() +
                             "&timestamp=" + MainActivity.currentAccount.getModified()), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public LoggedInUser isLogin(String username, String password) throws Exception {
        LoggedInUser fakeUsr = null;
        CallApi callApi = new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
            }
        });
        try {
            TaskObject result = callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/account_login?email=" + username + "&password=" + password), null))
                    .get();
            if (result != null && result.getBody() != null) {
                if (result.getBody().equals("NO_ACCOUNT")) {
                    //TODO: устаревшее
                    MainActivity.dataBaseEntityService.createAccount(username, password);
                } else if (result.getBody().equals("INVALID_PASSWORD")) {
                    throw new Exception("INVALID_PASSWORD");
                } else {
                    MDBAccount account = JSONtoClass(result.getBody(), MDBAccount.class);
                    if (account != null && account.getId() != null) {
                        MainActivity.dataBaseEntityService.saveAccount(new CurrentAccount(account));
                        MainActivity.dataBaseEntityService.loadCurrentAccount();
                    }
                }
            } else
                Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.no_connect), Toast.LENGTH_SHORT).show();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new LoggedInUser(MainActivity.currentAccount.getIdUUID().toString(), MainActivity.currentAccount.getEmail());
    }

    public void changePasswordAccountToServer(UUID id, String password) {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/account_change_password?id=" + id + "&password=" + password), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public <T> T JSONtoClass(String json, Class<T> tClass) {
        JsonParser parser = new JsonParser();
        JsonElement mJson =  parser.parse(json);
        Gson gson = new Gson();
        return tClass.cast(gson.fromJson(mJson, tClass));
    }

    public <T> List<T> JSONtoListClass(String json, Class<T> tClas) {
        Type listType = TypeToken.getParameterized(List.class, tClas).getType();
        return new Gson().fromJson(json, listType);
    }

    public String classToJSON(Object object) {
        GsonBuilder builder = new GsonBuilder().disableHtmlEscaping();
        Gson gson = builder.create();
        return gson.toJson(object);
    }

    public void exportRequestmeToServer(Requestme requestme) {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.POST,
                    new URL(getBaseUrl() + "/rest/requestme"), classToJSON(new MDBRequestme(requestme))));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void updateRequestsme() {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                if (callbackResponse != null && callbackResponse.getBody() != null) {
                    List<MDBRequestme> mdbRequestmes = JSONtoListClass(callbackResponse.getBody(), MDBRequestme.class);
                    //обновление и добавление
                    for (int i = 0; i < mdbRequestmes.size(); i++) {
                        int finalI = i;
                        Requestme requestme = MainActivity.requestsmes.stream().filter(v -> v.getIdUUID().equals(mdbRequestmes.get(finalI).getId())).findFirst().orElse(new Requestme());
                        if (requestme.getIdUUID() != null) {
                            if (mdbRequestmes.get(i).getModified() > requestme.getModified()) {
                                requestme.setPermission(mdbRequestmes.get(i).isPermission());
                                requestme.setModified(mdbRequestmes.get(i).getModified());
                                MainActivity.dataBaseEntityService.saveRequestme(requestme);
                                MainActivity.dataBaseEntityService.loadRequestsMe();
                                MainActivity.dataBaseEntityService.loadCurrentAccount();
                            } else if (mdbRequestmes.get(i).getModified() < requestme.getModified()) {
                                exportRequestmeToServer(requestme);
                            }
                        } else {
                            MainActivity.dataBaseEntityService.saveRequestme(new Requestme(mdbRequestmes.get(i)));
                            MainActivity.dataBaseEntityService.loadRequestsMe();
                            MainActivity.dataBaseEntityService.loadCurrentAccount();
                        }
                    }
                    //Удаление
                    for (int i = 0; i < MainActivity.requestsmes.size(); i++) {
                        int finalI = i;
                        MDBRequestme mdbRequestme = mdbRequestmes.stream().filter(v -> v.getId().equals(MainActivity.requestsmes.get(finalI).getIdUUID())).findFirst().orElse(new MDBRequestme());
                        if (mdbRequestme.getId() == null) {
                            MainActivity.dataBaseEntityService.removeRequestme(MainActivity.requestsmes.get(i).getId());
                            MainActivity.dataBaseEntityService.loadRequestsMe();
                            MainActivity.dataBaseEntityService.loadCurrentAccount();
                        }
                    }
                } else
                    Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.no_connect), Toast.LENGTH_SHORT).show();
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/requestme_get_all_by_email" +
                            "?email=" + MainActivity.currentAccount.getEmail()), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void removeRequestmeToServer(UUID id) {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.POST,
                    new URL(getBaseUrl() + "/rest/requestme/" + id), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void exportPositionsToServer() {
        List<Position> positions = MainActivity.dataBaseEntityService.getAllPositions();
        List<MDBPosition> mdbPositions = new ArrayList<>();
        positions.forEach(v -> mdbPositions.add(new MDBPosition(v)));
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                if (callbackResponse != null && callbackResponse.getBody() != null) {
                    MainActivity.dataBaseEntityService.removePositions(positions);
                } else
                    Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.no_connect), Toast.LENGTH_SHORT).show();
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.POST,
                    new URL(getBaseUrl() + "/rest/positions"), classToJSON(mdbPositions)));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void getLocationMyFamily(UUID familyId) {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                if (callbackResponse != null && callbackResponse.getBody() != null) {
                    List<RelativeInLocation> relativeInLocations = JSONtoListClass(callbackResponse.getBody(), RelativeInLocation.class);
                    FragmentManager fm = MainActivity.thisStatic.getSupportFragmentManager();

                    //if you added fragment via layout xml
                    /*LocationFragment fragment = (LocationFragment) Objects.requireNonNull(fm.findFragmentById(R.id.nav_host_fragment_content_menu)).getChildFragmentManager().getFragments().get(0);
                    if (fragment != null)*/
                    Fragment fragment = Objects.requireNonNull(MainActivity.thisStatic.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_menu))
                            .getChildFragmentManager().getFragments().get(0);
                    if(fragment instanceof LocationFragment)
                        ((LocationFragment)fragment).drawLocationFamily(relativeInLocations);
                }
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/positions_by_family?accountId=" + MainActivity.currentAccount.getIdUUID() +
                            "&familyId=" + familyId), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void getRelativePath(UUID relativeId, Long start, Long end) {
        CallApi callApi=new CallApi(new AsyncResponse() {
            @Override
            public void processFinish(TaskObject callbackResponse) {
                if (callbackResponse != null && callbackResponse.getBody() != null) {
                    List<MDBPosition> positions = JSONtoListClass(callbackResponse.getBody(), MDBPosition.class);
                    PathOnMapActivity.drawPathOnMap(positions);
                }
            }
        });
        try {
            callApi.execute(new TaskObject(TaskType.valueOf(MainActivity.settings.getProtocol()), TaskType.GET,
                    new URL(getBaseUrl() + "/rest/path_by_relative?accountId=" + MainActivity.currentAccount.getIdUUID() +
                            "&relativeId=" + relativeId + "&start=" + start + "&end=" + end), null));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
