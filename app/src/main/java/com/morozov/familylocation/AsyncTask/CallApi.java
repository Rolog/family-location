package com.morozov.familylocation.AsyncTask;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class CallApi extends AsyncTask<TaskObject, Void, TaskObject> {
    public AsyncResponse delegate = null;//Call back interface

    public CallApi(AsyncResponse asyncResponse) {
        delegate = asyncResponse;//Assigning call back interface through constructor
    }

    @Override
    protected TaskObject doInBackground(TaskObject... taskObjects) {
        HttpURLConnection connection = null;
        try {
            URL url = taskObjects[0].getUrl();
            if (taskObjects[0].getProtocol().equals(TaskType.HTTPS)) {
                connection = (HttpsURLConnection) url.openConnection();
            }
            else if (taskObjects[0].getProtocol().equals(TaskType.HTTP)) {
                connection = (HttpURLConnection) url.openConnection();
            }

            if (taskObjects[0].getType().equals(TaskType.GET))
                connection.setRequestMethod(TaskType.GET.name());
            else if (taskObjects[0].getType().equals(TaskType.POST)) {
                connection.setRequestMethod(TaskType.POST.name());
                // Create the data
                //String myData = "message=Hello";
                // Enable writing
                connection.setDoOutput(true);
                // Write the data
                connection.getOutputStream().write(taskObjects[0].getBody().getBytes(StandardCharsets.UTF_8));
            }
            assert connection != null;
            connection.setConnectTimeout(15000);
            connection.setReadTimeout(15000);
            int response = connection.getResponseCode();

            if (response == HttpsURLConnection.HTTP_OK) {
                StringBuilder builder = new StringBuilder();

                try (BufferedReader reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()))) {

                    String line;

                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                return new TaskObject(taskObjects[0].getProtocol(), taskObjects[0].getType(), taskObjects[0].getUrl(), builder.toString());
            }
        } catch (Exception e ) {
            e.printStackTrace();
        }
        finally {
            connection.disconnect(); // Закрыть HttpURLConnection
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(TaskObject aVoid) {
        super.onPostExecute(aVoid);
        delegate.processFinish(aVoid);
    }
}
