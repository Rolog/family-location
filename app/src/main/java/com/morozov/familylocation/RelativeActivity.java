package com.morozov.familylocation;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.material.navigation.NavigationView;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Relative;
import com.morozov.familylocation.ui.familyandlocations.FamilyAndLocationsFragment;
import com.morozov.familylocation.ui.login.LoginFormState;

import java.util.Objects;
import java.util.UUID;

public class RelativeActivity extends AppCompatActivity {
    private Relative relative = new Relative();
    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MenuItem btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_relative);

        try {
            Bundle b = getIntent().getExtras();
            int id = b.getInt("id");
            relative = MainActivity.dataBaseEntityService.getRelative(id);
        } catch (Exception ignored) {}
        EditText email = findViewById(R.id.editRelativeEmail);
        EditText name = findViewById(R.id.editRelativeName);
        loginFormState.observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                if (btnSave != null)
                    btnSave.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    email.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    name.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });
        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginDataChanged(email.getText().toString(),
                        name.getText().toString());
            }
        };
        email.addTextChangedListener(afterTextChangedListener);
        name.addTextChangedListener(afterTextChangedListener);
        if (relative.getEmail() != null) {
            email.setText(relative.getEmail());
            email.setEnabled(false);
        }
        if (relative.getName() != null)
            name.setText(relative.getName());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.relative_menu, menu);
        btnSave = menu.findItem(R.id.btnSaveRelative);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btnSaveRelative) {
            saveRelative();
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveRelative() {
        EditText email = findViewById(R.id.editRelativeEmail);
        relative.setEmail(email.getText().toString());
        EditText name = findViewById(R.id.editRelativeName);
        relative.setName(name.getText().toString());
        ContentValues contentValues = new ContentValues();
        //Если заполнены все поля
        if (relative.getEmail() != null && !relative.getEmail().equals("") &&
            relative.getName() != null && !relative.getName().equals(""))
            //Редактирование родственника
            if (relative.getId() != null) {
                contentValues.put(DatabaseDescription.DBRelatives.COLUMN_NAME, relative.getName());
                MainActivity.thisStatic.getContentResolver().update(
                        DatabaseDescription.DBRelatives.buildContactUri(relative.getId()), contentValues, null, null);
                MainActivity.dataBaseEntityService.setAccountModifiedCurrent();
                MainActivity.dataBaseEntityService.loadCurrentAccount();
                MainActivity.restRequestsService.checkUpdateAccount();
                FamilyAndLocationsFragment.relativesArrayAdapter.notifyDataSetChanged();
                onSupportNavigateUp();
            } //Создание нового родственника
            else {
                contentValues.put(DatabaseDescription.DBRelatives.COLUMN_ID_UUID, UUID.randomUUID().toString());
                contentValues.put(DatabaseDescription.DBRelatives.COLUMN_EMAIL, relative.getEmail());
                contentValues.put(DatabaseDescription.DBRelatives.COLUMN_NAME, relative.getName());
                MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBRelatives.CONTENT_URI, contentValues);
                MainActivity.dataBaseEntityService.setAccountModifiedCurrent();
                MainActivity.dataBaseEntityService.loadCurrentAccount();
                MainActivity.restRequestsService.checkUpdateAccount();
                FamilyAndLocationsFragment.relativesArrayAdapter.notifyDataSetChanged();
                onSupportNavigateUp();
            }
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_name));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        return Patterns.EMAIL_ADDRESS.matcher(username).matches();
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && !password.equals("");
    }
}