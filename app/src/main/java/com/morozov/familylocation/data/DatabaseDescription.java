// DatabaseDescription.java
// Describes the table name and column names for this app's database,
// and other information required by the ContentProvider
package com.morozov.familylocation.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class DatabaseDescription {
   // ContentProvider's name: typically the package name
   public static final String AUTHORITY =
      "com.morozov.familylocation.data";

   // base URI used to interact with the ContentProvider
   private static final Uri BASE_CONTENT_URI =
      Uri.parse("content://" + AUTHORITY);

   // nested class defines contents of the contacts table
   public static final class DBRelatives implements BaseColumns {
      public static final String TABLE_NAME = "relatives"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
         BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_NAME = "name";
      public static final String COLUMN_EMAIL = "email";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Семья
   public static final class DBFamilies implements BaseColumns {
      public static final String TABLE_NAME = "families"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_NAME = "name";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Родственники в семье
   public static final class DBFamiliesRelatives implements BaseColumns {
      public static final String TABLE_NAME = "families_relatives"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_ID_FAMILY = "idFamily";
      public static final String COLUMN_ID_RELATIVE = "idRelative";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Точки
   public static final class DBPoints implements BaseColumns {
      public static final String TABLE_NAME = "points"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_NUMBER = "number";
      public static final String COLUMN_ID_LOCATION = "idLocation";
      public static final String COLUMN_LAT = "lat";
      public static final String COLUMN_LON = "lon";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Локации
   public static final class DBLocations implements BaseColumns {
      public static final String TABLE_NAME = "locations"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_NAME = "name";
      public static final String COLUMN_TYPE = "type";
      public static final String COLUMN_RADIUS = "radius";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Аккаунт
   public static final class DBAccount implements BaseColumns {
      public static final String TABLE_NAME = "accounts"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_EMAIL = "email";
      public static final String COLUMN_PASSWORD = "password";
      public static final String COLUMN_MODIFIED = "modified";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Запросы на отслеживание
   public static final class DBRequestsme implements BaseColumns {
      public static final String TABLE_NAME = "requestsme"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_ID_WHO_UUID = "idWhoUUID";
      public static final String COLUMN_EMAIL_WHO = "emailWho";
      public static final String COLUMN_EMAIL_REQUEST = "emailRequest";
      public static final String COLUMN_PERMISSION = "permission";
      public static final String COLUMN_MODIFIED = "modified";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Настройки
   public static final class DBSettings implements BaseColumns {
      public static final String TABLE_NAME = "settings"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_PROTOCOL = "protocol";
      public static final String COLUMN_URL = "url";
      public static final String COLUMN_PORT = "port";
      public static final String COLUMN_TIME_UPDATE = "timeUpdate";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //Позиция GPS
   public static final class DBPositions implements BaseColumns {
      public static final String TABLE_NAME = "positions"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

      // column names for table's columns
      public static final String COLUMN_ID_UUID = "idUUID";
      public static final String COLUMN_ACCOUNT_ID = "accountId";
      public static final String COLUMN_ACCOUNT_EMAIL = "accountEmail";
      public static final String COLUMN_LAT = "lat";
      public static final String COLUMN_LON = "lon";
      public static final String COLUMN_ALTITUDE = "altitude";
      public static final String COLUMN_SPEED = "speed";
      public static final String COLUMN_MODIFIED = "modified";

      // creates a Uri for a specific contact
      public static Uri buildContactUri(long id) {
         return ContentUris.withAppendedId(CONTENT_URI, id);
      }
   }

   //JOIN
   public static final class DBJoin implements BaseColumns {
      public static final String TABLE_NAME = "join_fr"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
   }

   //DELETE ALL
   public static final class DBDeleteAll implements BaseColumns {
      public static final String TABLE_NAME = "delete_all"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
   }

   //DELETE Settings
   public static final class DBDeleteSettings implements BaseColumns {
      public static final String TABLE_NAME = "delete_settings"; // table's name

      // Uri for the contacts table
      public static final Uri CONTENT_URI =
              BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();
   }
}