package com.morozov.familylocation.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.enteties.CurrentAccount;
import com.morozov.familylocation.enteties.Family;
import com.morozov.familylocation.enteties.Location;
import com.morozov.familylocation.enteties.Point;
import com.morozov.familylocation.enteties.Position;
import com.morozov.familylocation.enteties.Relative;
import com.morozov.familylocation.enteties.Requestme;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DataBaseEntityService {

    @SuppressLint({"Range", "Recycle"})
    public void loadCurrentAccount() {
        clearCurrentAccount();
        int COLUMN_ID;
        UUID COLUMN_IDUUID;
        String COLUMN_NAME;
        String COLUMN_EMAIL;
        boolean COLUMN_PERMISSION;
        int COLUMN_ID1;
        UUID COLUMN_IDUUID1;
        String COLUMN_NAME1;

        //Аккаунт
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBAccount.CONTENT_URI, null, null, null, "1");
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            MainActivity.currentAccount.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBAccount._ID)));
            MainActivity.currentAccount.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBAccount.COLUMN_ID_UUID))));
            MainActivity.currentAccount.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBAccount.COLUMN_EMAIL)));
            MainActivity.currentAccount.setPassword(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBAccount.COLUMN_PASSWORD)));
            MainActivity.currentAccount.setModified(cursor.getLong(cursor.getColumnIndex(DatabaseDescription.DBAccount.COLUMN_MODIFIED)));

            //Родственники
            cursor = MainActivity.thisStatic.getContentResolver().query(
                    DatabaseDescription.DBRelatives.CONTENT_URI, null, null, null, null);
            while (cursor.moveToNext()) {
                COLUMN_ID = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRelatives._ID));
                COLUMN_IDUUID = UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_ID_UUID)));
                COLUMN_NAME = cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_NAME));
                COLUMN_EMAIL = cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_EMAIL));
                COLUMN_PERMISSION = getRelativePermission(COLUMN_EMAIL);
                MainActivity.currentAccount.getRelatives().add(new Relative(COLUMN_ID, COLUMN_IDUUID, COLUMN_EMAIL, COLUMN_NAME, COLUMN_PERMISSION));
            }

            //Семьи
            cursor = MainActivity.thisStatic.getContentResolver().query(
                    DatabaseDescription.DBFamilies.CONTENT_URI, null, null, null, null);
            while (cursor.moveToNext()) {
                COLUMN_ID = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBFamilies._ID));
                COLUMN_IDUUID = UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBFamilies.COLUMN_ID_UUID)));
                COLUMN_NAME = cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBFamilies.COLUMN_NAME));
                List<Relative> familyRelatives = new ArrayList<>();
                Cursor cursor1 = MainActivity.thisStatic.getContentResolver().query(
                        DatabaseDescription.DBJoin.CONTENT_URI.buildUpon()
                                .appendEncodedPath("families_relatives as fl inner join relatives as f on fl.idRelative = f._id").build(),
                        new String[]{"f._id as id", "f.idUUID as idUUID", "f.name as name", "f.email as email"},
                        DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_FAMILY + "='" + COLUMN_ID + "'", null, null);
                while (cursor1.moveToNext()) {
                    COLUMN_ID1 = cursor1.getInt(cursor1.getColumnIndex("id"));
                    COLUMN_IDUUID1 = UUID.fromString(cursor1.getString(cursor1.getColumnIndex("idUUID")));
                    COLUMN_NAME1 = cursor1.getString(cursor1.getColumnIndex("name"));
                    COLUMN_EMAIL = cursor1.getString(cursor1.getColumnIndex("email"));
                    COLUMN_PERMISSION = getRelativePermission(COLUMN_EMAIL);
                    familyRelatives.add(new Relative(COLUMN_ID1, COLUMN_IDUUID1, COLUMN_EMAIL, COLUMN_NAME1, COLUMN_PERMISSION));
                }
                MainActivity.currentAccount.getFamilies().add(new Family(COLUMN_ID, COLUMN_IDUUID, COLUMN_NAME, familyRelatives));
                cursor1.close();
            }

            //Локации
            cursor = MainActivity.thisStatic.getContentResolver().query(
                    DatabaseDescription.DBLocations.CONTENT_URI, null, null, null, null);
            while (cursor.moveToNext()) {
                COLUMN_ID = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBLocations._ID));
                COLUMN_IDUUID = UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBLocations.COLUMN_ID_UUID)));
                COLUMN_NAME = cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBLocations.COLUMN_NAME));
                List<Point> points = new ArrayList<>();
                Cursor cursor1 = MainActivity.thisStatic.getContentResolver().query(
                        DatabaseDescription.DBPoints.CONTENT_URI, null,
                        DatabaseDescription.DBPoints.COLUMN_ID_LOCATION + "='" + COLUMN_ID + "'", null,
                        DatabaseDescription.DBPoints.COLUMN_NUMBER + " ASC");
                while (cursor1.moveToNext()) {
                    points.add(new Point(cursor1.getInt(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_NUMBER)),
                            cursor1.getInt(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_ID_LOCATION)),
                            cursor1.getDouble(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_LAT)),
                            cursor1.getDouble(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_LON))));
                }
                MainActivity.currentAccount.getLocations().add(new Location(COLUMN_ID, COLUMN_IDUUID, COLUMN_NAME, points));
                cursor1.close();
            }
            cursor.close();
        }
    }

    public void initCurrentAccount() {
        MainActivity.currentAccount = new CurrentAccount();
        MainActivity.currentAccount.setFamilies(new ArrayList<>());
        MainActivity.currentAccount.setRelatives(new ArrayList<>());
        MainActivity.currentAccount.setLocations(new ArrayList<>());
    }

    public void clearCurrentAccount() {
        MainActivity.currentAccount.setId(null);
        MainActivity.currentAccount.setIdUUID(null);
        MainActivity.currentAccount.setEmail(null);
        MainActivity.currentAccount.setPassword(null);
        MainActivity.currentAccount.getFamilies().clear();
        MainActivity.currentAccount.getRelatives().clear();
        MainActivity.currentAccount.getLocations().clear();
        MainActivity.currentAccount.setModified(null);
    }

    @SuppressLint("Range")
    public Relative getRelative(int id) {
        Relative relative = new Relative();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBRelatives.buildContactUri(id), null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            relative.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRelatives._ID)));
            relative.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_ID_UUID))));
            relative.setEmail(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_EMAIL)));
            relative.setName(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_NAME)));
            relative.setPermission(getRelativePermission(relative.getEmail()));
            cursor.close();
        }
        return relative;
    }

    @SuppressLint("Range")
    public Family getFamily(int id) {
        Family family  = new Family();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBFamilies.buildContactUri(id), null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            family.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBFamilies._ID)));
            family.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBFamilies.COLUMN_ID_UUID))));
            family.setName(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBFamilies.COLUMN_NAME)));
            List<Relative> familyRelatives = new ArrayList<>();
            Cursor cursor1 = MainActivity.thisStatic.getContentResolver().query(
                    DatabaseDescription.DBJoin.CONTENT_URI.buildUpon()
                            .appendEncodedPath("families_relatives as fl inner join relatives as f on fl.idRelative = f._id").build(),
                    new String[]{"f._id as id", "f.idUUID as idUUID", "f.name as name", "f.email as email"},
                    DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_FAMILY + "='" + family.getId() + "'", null, null);
            while (cursor1.moveToNext()) {
                familyRelatives.add(new Relative(cursor1.getInt(cursor1.getColumnIndex("id")),
                        UUID.fromString(cursor1.getString(cursor1.getColumnIndex("idUUID"))),
                        cursor1.getString(cursor1.getColumnIndex("email")),
                        cursor1.getString(cursor1.getColumnIndex("name")),
                        getRelativePermission(cursor1.getString(cursor1.getColumnIndex("email")))));
            }
            family.setRelatives(familyRelatives);
            cursor1.close();
            cursor.close();
        }
        return family;
    }

    @SuppressLint("Range")
    public List<Relative> getAllRelatives() {
        List<Relative> relatives = new ArrayList<>();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBRelatives.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            relatives.add(new Relative(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRelatives._ID)),
                    UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_ID_UUID))),
                    cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_EMAIL)),
                    cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_NAME)),
                    getRelativePermission(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRelatives.COLUMN_EMAIL)))));
        }
        cursor.close();
        return relatives;
    }

    @SuppressLint("Range")
    public void removeFamilyRelativesByFamilyId(Integer familyId) {
        int CURSOR_ID;
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBFamiliesRelatives.CONTENT_URI,
                null,
                DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_FAMILY + "='" + familyId + "'", null, null);
        while (cursor.moveToNext()) {
            CURSOR_ID = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBFamiliesRelatives._ID));
            MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBFamiliesRelatives.buildContactUri(CURSOR_ID), null, null);
        }
        cursor.close();
    }

    public void removeAccount() {
        MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBDeleteAll.CONTENT_URI, null, null);
    }

    public void saveAccount(CurrentAccount account) {
        removeAccount();
        //Аккаунт
        ContentValues values = new ContentValues();
        values.put(DatabaseDescription.DBAccount.COLUMN_ID_UUID, account.getIdUUID().toString());
        values.put(DatabaseDescription.DBAccount.COLUMN_EMAIL, account.getEmail());
        //values.put(DatabaseDescription.DBAccount.COLUMN_PASSWORD, account.getPassword());
        values.put(DatabaseDescription.DBAccount.COLUMN_MODIFIED, account.getModified());
        MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBAccount.CONTENT_URI, values);
        //Родственники
        account.getRelatives().forEach(v -> {
            ContentValues values1 = new ContentValues();
            values1.put(DatabaseDescription.DBRelatives.COLUMN_ID_UUID, v.getIdUUID().toString());
            values1.put(DatabaseDescription.DBRelatives.COLUMN_EMAIL, v.getEmail());
            values1.put(DatabaseDescription.DBRelatives.COLUMN_NAME, v.getName());
            MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBRelatives.CONTENT_URI, values1);
        });
        //Семьи
        List<Relative> relatives = getAllRelatives();
        account.getFamilies().forEach(v -> {
            ContentValues values1 = new ContentValues();
            values1.put(DatabaseDescription.DBFamilies.COLUMN_ID_UUID, v.getIdUUID().toString());
            values1.put(DatabaseDescription.DBFamilies.COLUMN_NAME, v.getName());
            Uri newFamily = MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBFamilies.CONTENT_URI, values1);
            v.getRelatives().forEach(v1 -> {
                ContentValues values2 = new ContentValues();
                values2.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_UUID, UUID.randomUUID().toString());
                values2.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_FAMILY, Integer.valueOf(newFamily.getLastPathSegment()));
                Relative relative = relatives.stream().filter(f -> f.getIdUUID().equals(v1.getIdUUID())).findFirst().orElse(null);
                if (relative != null)
                    values2.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_RELATIVE, relative.getId());
                MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBFamiliesRelatives.CONTENT_URI, values2);
            });
        });
        //Локации
        account.getLocations().forEach(v -> {
            ContentValues values1 = new ContentValues();
            values1.put(DatabaseDescription.DBLocations.COLUMN_ID_UUID, v.getIdUUID().toString());
            values1.put(DatabaseDescription.DBLocations.COLUMN_NAME, v.getName());
            Uri newLocation = MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBLocations.CONTENT_URI, values1);
            v.getPoints().forEach(v1 -> {
                ContentValues values2 = new ContentValues();
                values2.put(DatabaseDescription.DBPoints.COLUMN_NUMBER, v1.getNumber());
                values2.put(DatabaseDescription.DBPoints.COLUMN_ID_LOCATION, Integer.valueOf(newLocation.getLastPathSegment()));
                values2.put(DatabaseDescription.DBPoints.COLUMN_LAT, v1.getLat());
                values2.put(DatabaseDescription.DBPoints.COLUMN_LON, v1.getLon());
                MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBPoints.CONTENT_URI, values2);
            });
        });

    }

    public void createAccount(String username, String password) {
        ContentValues values = new ContentValues();
        values.put(DatabaseDescription.DBAccount.COLUMN_ID_UUID, UUID.randomUUID().toString());
        values.put(DatabaseDescription.DBAccount.COLUMN_EMAIL, username);
        //values.put(DatabaseDescription.DBAccount.COLUMN_PASSWORD, password);
        values.put(DatabaseDescription.DBAccount.COLUMN_MODIFIED, System.currentTimeMillis());
        MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBAccount.CONTENT_URI, values);
    }

    @SuppressLint("Range")
    public Location getLocation(int id) {
        Location location  = new Location();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBLocations.buildContactUri(id), null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            location.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBLocations._ID)));
            location.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBLocations.COLUMN_ID_UUID))));
            location.setName(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBLocations.COLUMN_NAME)));
            List<Point> points = new ArrayList<>();
            Cursor cursor1 = MainActivity.thisStatic.getContentResolver().query(
                    DatabaseDescription.DBPoints.CONTENT_URI, null,
                    DatabaseDescription.DBPoints.COLUMN_ID_LOCATION + "='" + location.getId() + "'", null,
                    DatabaseDescription.DBPoints.COLUMN_NUMBER + " ASC");
            while (cursor1.moveToNext()) {
                points.add(new Point(cursor1.getInt(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_NUMBER)),
                        cursor1.getInt(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_ID_LOCATION)),
                        cursor1.getDouble(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_LAT)),
                        cursor1.getDouble(cursor1.getColumnIndex(DatabaseDescription.DBPoints.COLUMN_LON))));
            }
            location.setPoints(points);
            cursor1.close();
            cursor.close();
        }
        return location;
    }

    @SuppressLint("Range")
    public void removePointsByLocationId(Integer locationId) {
        int CURSOR_ID;
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBPoints.CONTENT_URI,
                null,
                DatabaseDescription.DBPoints.COLUMN_ID_LOCATION + "='" + locationId + "'", null, null);
        while (cursor.moveToNext()) {
            CURSOR_ID = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBPoints._ID));
            MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBPoints.buildContactUri(CURSOR_ID), null, null);
        }
        cursor.close();
    }

    public void setAccountModifiedCurrent() {
        ContentValues values = new ContentValues();
        values.put(DatabaseDescription.DBAccount.COLUMN_MODIFIED, System.currentTimeMillis());
        MainActivity.thisStatic.getContentResolver()
                .update(DatabaseDescription.DBAccount.buildContactUri(MainActivity.currentAccount.getId()), values, null, null);
    }

    public void setAccountModified0() {
        ContentValues values = new ContentValues();
        values.put(DatabaseDescription.DBAccount.COLUMN_MODIFIED, 0);
        MainActivity.thisStatic.getContentResolver()
                .update(DatabaseDescription.DBAccount.buildContactUri(MainActivity.currentAccount.getId()), values, null, null);
    }

    @SuppressLint("Range")
    public void loadRequestsMe() {
        MainActivity.requestsmes.clear();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBRequestsme.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            Requestme requestsme = new Requestme();
            requestsme.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRequestsme._ID)));
            requestsme.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_ID_UUID))));
            requestsme.setIdWho(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_ID_WHO_UUID))));
            requestsme.setEmailWho(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_EMAIL_WHO)));
            requestsme.setEmailRequest(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_EMAIL_REQUEST)));
            requestsme.setPermission(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_PERMISSION)) == 1);
            requestsme.setModified(cursor.getLong(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_MODIFIED)));
            MainActivity.requestsmes.add(requestsme);
        }
        cursor.close();
    }

    @SuppressLint("Range")
    public Requestme getRequestsMe(int id) {
        Requestme requestsme = new Requestme();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBRequestsme.buildContactUri(id), null,
                null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            requestsme.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRequestsme._ID)));
            requestsme.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_ID_UUID))));
            requestsme.setIdWho(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_ID_WHO_UUID))));
            requestsme.setEmailWho(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_EMAIL_WHO)));
            requestsme.setEmailRequest(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_EMAIL_REQUEST)));
            requestsme.setPermission(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_PERMISSION)) == 1);
            requestsme.setModified(cursor.getLong(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_MODIFIED)));
            cursor.close();
        }
        return requestsme;
    }

    public void saveRequestme(Requestme requestme) {
        ContentValues values = new ContentValues();
        values.put(DatabaseDescription.DBRequestsme.COLUMN_ID_UUID, requestme.getIdUUID().toString());
        values.put(DatabaseDescription.DBRequestsme.COLUMN_ID_WHO_UUID, requestme.getIdWho().toString());
        values.put(DatabaseDescription.DBRequestsme.COLUMN_EMAIL_WHO, requestme.getEmailWho());
        values.put(DatabaseDescription.DBRequestsme.COLUMN_EMAIL_REQUEST, requestme.getEmailRequest());
        values.put(DatabaseDescription.DBRequestsme.COLUMN_PERMISSION, requestme.isPermission());
        values.put(DatabaseDescription.DBRequestsme.COLUMN_MODIFIED, requestme.getModified());
        if (requestme.getId() != null)
            MainActivity.thisStatic.getContentResolver().update(DatabaseDescription.DBRequestsme.buildContactUri(requestme.getId()), values, null, null);
        else
            MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBRequestsme.CONTENT_URI, values);
    }

    public void removeRequestme(int id) {
        MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBRequestsme.buildContactUri(id), null, null);
    }

    @SuppressLint("Range")
    public void loadSettings() {
        MainActivity.settings.setId(null);
        MainActivity.settings.setProtocol(null);
        MainActivity.settings.setUrl(null);
        MainActivity.settings.setPort(null);
        MainActivity.settings.setTimeUpdate(null);
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBSettings.CONTENT_URI, null, null, null, "1");
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            MainActivity.settings.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBSettings._ID)));
            MainActivity.settings.setProtocol(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBSettings.COLUMN_PROTOCOL)));
            MainActivity.settings.setUrl(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBSettings.COLUMN_URL)));
            if (!cursor.isNull(cursor.getColumnIndex(DatabaseDescription.DBSettings.COLUMN_PORT)))
                MainActivity.settings.setPort(cursor.getLong(cursor.getColumnIndex(DatabaseDescription.DBSettings.COLUMN_PORT)));
            if (!cursor.isNull(cursor.getColumnIndex(DatabaseDescription.DBSettings.COLUMN_TIME_UPDATE)))
                MainActivity.settings.setTimeUpdate(cursor.getLong(cursor.getColumnIndex(DatabaseDescription.DBSettings.COLUMN_TIME_UPDATE)));
            cursor.close();
        } else {
            if (MainActivity.settings.getProtocol() == null)
                MainActivity.settings.setProtocol(MainActivity.thisStatic.getString(R.string.default_protocol));
            if (MainActivity.settings.getUrl() == null)
                MainActivity.settings.setUrl(MainActivity.thisStatic.getString(R.string.default_url));
            if (MainActivity.settings.getPort() == null && !MainActivity.thisStatic.getString(R.string.default_port).equals(""))
                MainActivity.settings.setPort(Long.valueOf(MainActivity.thisStatic.getString(R.string.default_port)));
            if (MainActivity.settings.getTimeUpdate() == null && !MainActivity.thisStatic.getString(R.string.default_time_update).equals(""))
                MainActivity.settings.setTimeUpdate(Long.valueOf(MainActivity.thisStatic.getString(R.string.default_time_update)));
        }
    }

    public void savePosition(Position position) {
        ContentValues values = new ContentValues();
        values.put(DatabaseDescription.DBPositions.COLUMN_ID_UUID, position.getIdUUID().toString());
        values.put(DatabaseDescription.DBPositions.COLUMN_ACCOUNT_ID, position.getAccountId().toString());
        values.put(DatabaseDescription.DBPositions.COLUMN_ACCOUNT_EMAIL, position.getAccountEmail());
        values.put(DatabaseDescription.DBPositions.COLUMN_LAT, position.getLat());
        values.put(DatabaseDescription.DBPositions.COLUMN_LON, position.getLon());
        values.put(DatabaseDescription.DBPositions.COLUMN_ALTITUDE, position.getAltitude());
        values.put(DatabaseDescription.DBPositions.COLUMN_SPEED, position.getSpeed());
        values.put(DatabaseDescription.DBPositions.COLUMN_MODIFIED, position.getModified());
        if (position.getId() != null)
            MainActivity.thisStatic.getContentResolver().update(DatabaseDescription.DBPositions.buildContactUri(position.getId()), values, null, null);
        else
            MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBPositions.CONTENT_URI, values);
    }

    @SuppressLint("Range")
    public List<Position> getAllPositions() {
        List<Position> positions = new ArrayList<>();
        Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                DatabaseDescription.DBPositions.CONTENT_URI, null, null, null, null);
        while (cursor.moveToNext()) {
            Position position = new Position();
            position.setId(cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBPositions._ID)));
            position.setIdUUID(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_ID_UUID))));
            position.setAccountId(UUID.fromString(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_ACCOUNT_ID))));
            position.setAccountEmail(cursor.getString(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_ACCOUNT_EMAIL)));
            position.setLat(cursor.getDouble(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_LAT)));
            position.setLon(cursor.getDouble(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_LON)));
            position.setAltitude(cursor.getDouble(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_ALTITUDE)));
            position.setSpeed(cursor.getDouble(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_SPEED)));
            position.setModified(cursor.getLong(cursor.getColumnIndex(DatabaseDescription.DBPositions.COLUMN_MODIFIED)));
            positions.add(position);
        }
        cursor.close();
        return positions;
    }

    public void removePositions(List<Position> positions) {
        positions.forEach(v -> {
            MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBPositions.buildContactUri(v.getId()), null, null);
        });
    }

    @SuppressLint("Range")
    public Boolean getRelativePermission(String email) {
        boolean permission = false;
        if (MainActivity.currentAccount.getIdUUID() != null) {
            Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                    DatabaseDescription.DBRequestsme.CONTENT_URI, null,
                    DatabaseDescription.DBRequestsme.COLUMN_ID_WHO_UUID + "='" + MainActivity.currentAccount.getIdUUID().toString() + "' AND " +
                            DatabaseDescription.DBRequestsme.COLUMN_EMAIL_REQUEST + "='" + email + "'", null, "1");
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                permission = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBRequestsme.COLUMN_PERMISSION)) == 1;
                cursor.close();
            }
        }
        return permission;
    }
}
