// AddressBookDatabaseHelper.java
// SQLiteOpenHelper subclass that defines the app's database
package com.morozov.familylocation.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.morozov.familylocation.data.DatabaseDescription.*;

class DatabaseHelper extends SQLiteOpenHelper {
   private static final String DATABASE_NAME = "FamilyLocation.db";
   private static final int DATABASE_VERSION = 1;

   // constructor
   public DatabaseHelper(Context context) {
      super(context, DATABASE_NAME, null, DATABASE_VERSION);
   }

   // creates the contacts table when the database is created
   @Override
   public void onCreate(SQLiteDatabase db) {
      // SQL for creating the table
      String CREATE_TRACKS_TABLE =
         "CREATE TABLE " + DBRelatives.TABLE_NAME + "(" +
                 DBRelatives._ID + " integer primary key, " +
                 DBRelatives.COLUMN_ID_UUID + " TEXT, " +
                 DBRelatives.COLUMN_NAME + " TEXT, " +
                 DBRelatives.COLUMN_EMAIL + " TEXT);";
      db.execSQL(CREATE_TRACKS_TABLE); // create the table
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBFamilies.TABLE_NAME + "(" +
                      DBFamilies._ID + " integer primary key, " +
                      DBFamilies.COLUMN_ID_UUID + " TEXT, " +
                      DBFamilies.COLUMN_NAME + " TEXT);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBFamiliesRelatives.TABLE_NAME + "(" +
                      DBFamiliesRelatives._ID + " integer primary key, " +
                      DBFamiliesRelatives.COLUMN_ID_UUID + " TEXT, " +
                      DBFamiliesRelatives.COLUMN_ID_FAMILY + " integer, " +
                      DBFamiliesRelatives.COLUMN_ID_RELATIVE + " integer);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBPoints.TABLE_NAME + "(" +
                      DBPoints._ID + " integer primary key, " +
                      DBPoints.COLUMN_NUMBER + " integer, " +
                      DBPoints.COLUMN_ID_LOCATION + " integer, " +
                      DBPoints.COLUMN_LAT + " REAL, " +
                      DBPoints.COLUMN_LON + " REAL);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBLocations.TABLE_NAME + "(" +
                      DBLocations._ID + " integer primary key, " +
                      DBLocations.COLUMN_ID_UUID + " TEXT, " +
                      DBLocations.COLUMN_NAME + " TEXT, " +
                      DBLocations.COLUMN_TYPE + " TEXT, " +
                      DBLocations.COLUMN_RADIUS + " REAL);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBAccount.TABLE_NAME + "(" +
                      DBAccount._ID + " integer primary key, " +
                      DBAccount.COLUMN_ID_UUID + " TEXT, " +
                      DBAccount.COLUMN_EMAIL + " TEXT, " +
                      DBAccount.COLUMN_PASSWORD + " TEXT, " +
                      DBAccount.COLUMN_MODIFIED + " long);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBRequestsme.TABLE_NAME + "(" +
                      DBRequestsme._ID + " integer primary key, " +
                      DBRequestsme.COLUMN_ID_UUID + " TEXT, " +
                      DBRequestsme.COLUMN_ID_WHO_UUID + " TEXT, " +
                      DBRequestsme.COLUMN_EMAIL_WHO + " TEXT, " +
                      DBRequestsme.COLUMN_EMAIL_REQUEST + " TEXT, " +
                      DBRequestsme.COLUMN_PERMISSION + " integer, " +
                      DBRequestsme.COLUMN_MODIFIED + " long);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBSettings.TABLE_NAME + "(" +
                      DBSettings._ID + " integer primary key, " +
                      DBSettings.COLUMN_PROTOCOL + " TEXT, " +
                      DBSettings.COLUMN_URL + " TEXT, " +
                      DBSettings.COLUMN_PORT + " long, " +
                      DBSettings.COLUMN_TIME_UPDATE + " long);";
      db.execSQL(CREATE_TRACKS_TABLE);
      CREATE_TRACKS_TABLE =
              "CREATE TABLE " + DBPositions.TABLE_NAME + "(" +
                      DBPositions._ID + " integer primary key, " +
                      DBPositions.COLUMN_ID_UUID + " TEXT, " +
                      DBPositions.COLUMN_ACCOUNT_ID + " TEXT, " +
                      DBPositions.COLUMN_ACCOUNT_EMAIL + " TEXT, " +
                      DBPositions.COLUMN_LAT + " REAL, " +
                      DBPositions.COLUMN_LON + " REAL, " +
                      DBPositions.COLUMN_ALTITUDE + " REAL, " +
                      DBPositions.COLUMN_SPEED + " REAL, " +
                      DBPositions.COLUMN_MODIFIED + " long);";
      db.execSQL(CREATE_TRACKS_TABLE);
   }

   // normally defines how to upgrade the database when the schema changes
   @Override
   public void onUpgrade(SQLiteDatabase db, int oldVersion,
                         int newVersion) { }
}
