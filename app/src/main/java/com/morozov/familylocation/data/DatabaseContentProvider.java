// AddressBookContentProvider.java
// ContentProvider subclass for manipulating the app's database
package com.morozov.familylocation.data;

import static com.morozov.familylocation.data.DatabaseDescription.*;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Bundle;

import com.morozov.familylocation.R;

public class DatabaseContentProvider extends ContentProvider {
   // used to access the database
   private DatabaseHelper dbHelper;

   // UriMatcher helps ContentProvider determine operation to perform
   private static final UriMatcher uriMatcher =
      new UriMatcher(UriMatcher.NO_MATCH);

   // constants used with UriMatcher to determine operation to perform
   private static final int ONE_RELATIVE = 1; // manipulate one contact
   private static final int RELATIVES = 2; // manipulate contacts table
   private static final int ONE_FAMILY = 3;
   private static final int FAMILIES = 4;
   private static final int ONE_POINT = 5;
   private static final int POINTS = 6;
   private static final int ONE_LOCATION = 7;
   private static final int LOCATIONS = 8;
   private static final int ONE_FAMILIES_RELATIVES = 9;
   private static final int FAMILIES_RELATIVES = 10;
   private static final int JOIN = 11;
   private static final int ONE_ACCOUNT = 12;
   private static final int ACCOUNTS = 13;
   private static final int DELETE_ALL = 14;
   private static final int ONE_REQUESTSME = 15;
   private static final int REQUESTSME_ALL = 16;
   private static final int ONE_SETTING = 17;
   private static final int SETTINGS = 18;
   private static final int DELETE_SETTINGS = 19;
   private static final int ONE_POSITION = 20;
   private static final int POSITIONS = 21;

   // static block to configure this ContentProvider's UriMatcher
   static {
      // Uri for Contact with the specified id (#)
      uriMatcher.addURI(AUTHORITY,
         DBRelatives.TABLE_NAME + "/#", ONE_RELATIVE);

      // Uri for Contacts table
      uriMatcher.addURI(AUTHORITY,
         DBRelatives.TABLE_NAME, RELATIVES);

      uriMatcher.addURI(AUTHORITY,
              DBFamilies.TABLE_NAME + "/#", ONE_FAMILY);

      uriMatcher.addURI(AUTHORITY,
              DBFamilies.TABLE_NAME, FAMILIES);

      uriMatcher.addURI(AUTHORITY,
              DBPoints.TABLE_NAME + "/#", ONE_POINT);

      uriMatcher.addURI(AUTHORITY,
              DBPoints.TABLE_NAME, POINTS);

      uriMatcher.addURI(AUTHORITY,
              DBLocations.TABLE_NAME + "/#", ONE_LOCATION);

      uriMatcher.addURI(AUTHORITY,
              DBLocations.TABLE_NAME, LOCATIONS);

      uriMatcher.addURI(AUTHORITY,
              DBFamiliesRelatives.TABLE_NAME + "/#", ONE_FAMILIES_RELATIVES);

      uriMatcher.addURI(AUTHORITY,
              DBFamiliesRelatives.TABLE_NAME, FAMILIES_RELATIVES);

      uriMatcher.addURI(AUTHORITY,
              DBJoin.TABLE_NAME + "/*", JOIN);

      uriMatcher.addURI(AUTHORITY,
              DBAccount.TABLE_NAME + "/#", ONE_ACCOUNT);

      uriMatcher.addURI(AUTHORITY,
              DBAccount.TABLE_NAME, ACCOUNTS);

      uriMatcher.addURI(AUTHORITY,
              DBDeleteAll.TABLE_NAME, DELETE_ALL);

      uriMatcher.addURI(AUTHORITY,
              DBRequestsme.TABLE_NAME + "/#", ONE_REQUESTSME);

      uriMatcher.addURI(AUTHORITY,
              DBRequestsme.TABLE_NAME, REQUESTSME_ALL);

      uriMatcher.addURI(AUTHORITY,
              DBSettings.TABLE_NAME + "/#", ONE_SETTING);

      uriMatcher.addURI(AUTHORITY,
              DBSettings.TABLE_NAME, SETTINGS);

      uriMatcher.addURI(AUTHORITY,
              DBDeleteSettings.TABLE_NAME, DELETE_SETTINGS);

      uriMatcher.addURI(AUTHORITY,
              DBPositions.TABLE_NAME + "/#", ONE_POSITION);

      uriMatcher.addURI(AUTHORITY,
              DBPositions.TABLE_NAME, POSITIONS);
   }

   // called when the AddressBookContentProvider is created
   @Override
   public boolean onCreate() {
      // create the AddressBookDatabaseHelper
      dbHelper = new DatabaseHelper(getContext());
      return true; // ContentProvider successfully created
   }

   // required method: Not used in this app, so we return null
   @Override
   public String getType(Uri uri) {
      return null;
   }

   // query the database
   @Override
   public Cursor query(Uri uri, String[] projection,
                       String selection, String[] selectionArgs, String sortOrder) {
      String TABLE_NAME = uri.getPath().split("/")[1];
      // create SQLiteQueryBuilder for querying contacts table
      SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
      queryBuilder.setTables(TABLE_NAME);


      switch (uriMatcher.match(uri)) {
         case ONE_RELATIVE: // contact with specified id will be selected
            queryBuilder.appendWhere(
                    DBRelatives._ID + "=" + uri.getLastPathSegment());
            break;
         case RELATIVES: // all contacts will be selected
            break;
         case ONE_FAMILY: // contact with specified id will be selected
            queryBuilder.appendWhere(
                    DBFamilies._ID + "=" + uri.getLastPathSegment());
            break;
         case FAMILIES: // all contacts will be selected
            break;
         case ONE_POINT:
            queryBuilder.appendWhere(
                    DBPoints._ID + "=" + uri.getLastPathSegment());
            break;
         case POINTS:
            break;
         case ONE_LOCATION:
            queryBuilder.appendWhere(
                    DBLocations._ID + "=" + uri.getLastPathSegment());
            break;
         case LOCATIONS:
            break;
         case ONE_FAMILIES_RELATIVES:
            queryBuilder.appendWhere(
                    DBFamiliesRelatives._ID + "=" + uri.getLastPathSegment());
            break;
         case FAMILIES_RELATIVES:
            break;
         case JOIN:
            String table = uri.getLastPathSegment();
            Cursor cursor1 = dbHelper.getReadableDatabase().query(table, projection, selection, selectionArgs, null, null, sortOrder);
            cursor1.setNotificationUri(getContext().getContentResolver(), uri);
            return cursor1;
         case ONE_ACCOUNT:
            queryBuilder.appendWhere(
                    DBAccount._ID + "=" + uri.getLastPathSegment());
            break;
         case ACCOUNTS:
            break;
         case ONE_REQUESTSME:
            queryBuilder.appendWhere(
                    DBRequestsme._ID + "=" + uri.getLastPathSegment());
            break;
         case REQUESTSME_ALL:
            break;
         case ONE_SETTING:
            queryBuilder.appendWhere(
                    DBSettings._ID + "=" + uri.getLastPathSegment());
            break;
         case SETTINGS:
            break;
         case ONE_POSITION:
            queryBuilder.appendWhere(
                    DBPositions._ID + "=" + uri.getLastPathSegment());
            break;
         case POSITIONS:
            break;
         default:
            throw new UnsupportedOperationException(
               getContext().getString(R.string.invalid_query_uri) + uri);
      }

      // execute the query to select one or all contacts
      Cursor cursor = queryBuilder.query(dbHelper.getReadableDatabase(),
         projection, selection, selectionArgs, null, null, sortOrder);

      // configure to watch for content changes
      cursor.setNotificationUri(getContext().getContentResolver(), uri);
      return cursor;
   }

   // insert a new contact in the database
   @Override
   public Uri insert(Uri uri, ContentValues values) {
      Uri newContactUri = null;
      String TABLE_NAME = uri.getPath().split("/")[1];
      long rowId = dbHelper.getWritableDatabase().insert(
              TABLE_NAME, null, values);
      switch (uriMatcher.match(uri)) {
         case RELATIVES:
            // if the contact was inserted, create an appropriate Uri;
            // otherwise, throw an exception
            if (rowId > 0) { // SQLite row IDs start at 1
               newContactUri = DBRelatives.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                  getContext().getString(R.string.insert_failed) + uri);
            break;
         case FAMILIES:
            if (rowId > 0) { // SQLite row IDs start at 1
               newContactUri = DBFamilies.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case POINTS:
            if (rowId > 0) {
               newContactUri = DBPoints.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case LOCATIONS:
            if (rowId > 0) {
               newContactUri = DBLocations.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case FAMILIES_RELATIVES:
            if (rowId > 0) {
               newContactUri = DBFamiliesRelatives.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case ACCOUNTS:
            if (rowId > 0) {
               newContactUri = DBAccount.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case REQUESTSME_ALL:
            if (rowId > 0) {
               newContactUri = DBRequestsme.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case SETTINGS:
            if (rowId > 0) {
               newContactUri = DBSettings.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         case POSITIONS:
            if (rowId > 0) {
               newContactUri = DBPositions.buildContactUri(rowId);

               // notify observers that the database changed
               getContext().getContentResolver().notifyChange(uri, null);
            }
            else
               throw new SQLException(
                       getContext().getString(R.string.insert_failed) + uri);
            break;
         default:
            throw new UnsupportedOperationException(
               getContext().getString(R.string.invalid_insert_uri) + uri);
      }

      return newContactUri;
   }

   // update an existing contact in the database
   @Override
   public int update(Uri uri, ContentValues values,
                     String selection, String[] selectionArgs) {
      int numberOfRowsUpdated; // 1 if update successful; 0 otherwise
      String id = uri.getLastPathSegment();
      switch (uriMatcher.match(uri)) {
         case ONE_RELATIVE:
            // update the contact
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBRelatives.TABLE_NAME, values, DBRelatives._ID + "=" + id,
               selectionArgs);
            break;
         case ONE_FAMILY:
            // update the contact
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBFamilies.TABLE_NAME, values, DBFamilies._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_POINT:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBPoints.TABLE_NAME, values, DBPoints._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_LOCATION:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBLocations.TABLE_NAME, values, DBLocations._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_FAMILIES_RELATIVES:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBFamiliesRelatives.TABLE_NAME, values, DBFamiliesRelatives._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_ACCOUNT:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBAccount.TABLE_NAME, values, DBAccount._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_REQUESTSME:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBRequestsme.TABLE_NAME, values, DBRequestsme._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_SETTING:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBSettings.TABLE_NAME, values, DBSettings._ID + "=" + id,
                    selectionArgs);
            break;
         case ONE_POSITION:
            numberOfRowsUpdated = dbHelper.getWritableDatabase().update(
                    DBPositions.TABLE_NAME, values, DBPositions._ID + "=" + id,
                    selectionArgs);
            break;
         default:
            throw new UnsupportedOperationException(
               getContext().getString(R.string.invalid_update_uri) + uri);
      }

      // if changes were made, notify observers that the database changed
      if (numberOfRowsUpdated != 0) {
         getContext().getContentResolver().notifyChange(uri, null);
      }

      return numberOfRowsUpdated;
   }

   // delete an existing contact from the database
   @Override
   public int delete(Uri uri, String selection, String[] selectionArgs) {
      int numberOfRowsDeleted;
      String id = uri.getLastPathSegment();
      switch (uriMatcher.match(uri)) {
         case ONE_RELATIVE:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBRelatives.TABLE_NAME, DBRelatives._ID + "=" + id, selectionArgs);
            break;
         case ONE_FAMILY:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBFamilies.TABLE_NAME, DBFamilies._ID + "=" + id, selectionArgs);
            break;
         case ONE_POINT:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBPoints.TABLE_NAME, DBPoints._ID + "=" + id, selectionArgs);
            break;
         case ONE_LOCATION:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBLocations.TABLE_NAME, DBLocations._ID + "=" + id, selectionArgs);
            break;
         case ONE_FAMILIES_RELATIVES:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBFamiliesRelatives.TABLE_NAME, DBFamiliesRelatives._ID + "=" + id, selectionArgs);
            break;
         case ONE_ACCOUNT:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBAccount.TABLE_NAME, DBAccount._ID + "=" + id, selectionArgs);
            break;
         case DELETE_ALL:
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBAccount.TABLE_NAME);
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBFamilies.TABLE_NAME);
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBFamiliesRelatives.TABLE_NAME);
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBRelatives.TABLE_NAME);
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBLocations.TABLE_NAME);
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBPoints.TABLE_NAME);
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBRequestsme.TABLE_NAME);
            numberOfRowsDeleted = 0;
            break;
         case DELETE_SETTINGS:
            dbHelper.getWritableDatabase().execSQL("delete from "+ DBSettings.TABLE_NAME);
            numberOfRowsDeleted = 0;
            break;
         case ONE_REQUESTSME:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBRequestsme.TABLE_NAME, DBRequestsme._ID + "=" + id, selectionArgs);
            break;
         case ONE_SETTING:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBSettings.TABLE_NAME, DBSettings._ID + "=" + id, selectionArgs);
            break;
         case ONE_POSITION:
            // delete the contact
            numberOfRowsDeleted = dbHelper.getWritableDatabase().delete(
                    DBPositions.TABLE_NAME, DBPositions._ID + "=" + id, selectionArgs);
            break;
         default:
            throw new UnsupportedOperationException(
               getContext().getString(R.string.invalid_delete_uri) + uri);
      }

      // notify observers that the database changed
      if (numberOfRowsDeleted != 0) {
         getContext().getContentResolver().notifyChange(uri, null);
      }

      return numberOfRowsDeleted;
   }
}
