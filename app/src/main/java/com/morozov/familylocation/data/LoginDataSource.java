package com.morozov.familylocation.data;

import android.content.Intent;
import android.widget.TextView;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.Utils.Constants;
import com.morozov.familylocation.data.model.LoggedInUser;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            //TODO: Сделать сначала проверку аккаунта на удаленном сервере
            return new Result.Success<>(MainActivity.restRequestsService.isLogin(username, password));
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        MainActivity.dataBaseEntityService.clearCurrentAccount();
        MainActivity.dataBaseEntityService.removeAccount();
    }
}