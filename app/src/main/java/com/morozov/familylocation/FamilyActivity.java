package com.morozov.familylocation;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.morozov.familylocation.Utils.Constants;
import com.morozov.familylocation.Utils.FamilyRelativesArrayAdapter;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Family;
import com.morozov.familylocation.enteties.Relative;
import com.morozov.familylocation.ui.familyandlocations.FamilyAndLocationsFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import androidx.activity.result.ActivityResult;

public class FamilyActivity extends AppCompatActivity {
    private Family family = new Family();
    private FamilyRelativesArrayAdapter familyRelativesArrayAdapter;
    private ListView relativesListView; // Для вывода информации

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_family);

        family.setRelatives(new ArrayList<>());
        try {
            Bundle b = getIntent().getExtras();
            int id = b.getInt("id");
            family = MainActivity.dataBaseEntityService.getFamily(id);
        } catch (Exception ignored) {}
        EditText name = findViewById(R.id.editFamilyName);
        if (family.getName() != null)
            name.setText(family.getName());

        relativesListView = this.findViewById(R.id.listRelativesForFamily);
        familyRelativesArrayAdapter = new FamilyRelativesArrayAdapter(this, family.getRelatives());
        relativesListView.setAdapter(familyRelativesArrayAdapter);

        ActivityResultLauncher<Intent> activityResultLaunch = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Constants.RESULT_OK) {
                            assert result.getData() != null;
                            Bundle b = result.getData().getExtras();
                            int count = b.getInt("count");
                            List<Integer> ids = new ArrayList<>();
                            for (int i = 0; i < count; i++) {
                                ids.add(b.getInt("id_" + i));
                            }
                            addRelatives(ids);
                        }
                    }
                });

        ImageButton btnSelectRelative = this.findViewById(R.id.btnSelectRelative);
        btnSelectRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.thisStatic, SelectRelativesActivity.class);
                activityResultLaunch.launch(intent);
            }
        });
    }

    public void doSomeOperations() {}

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.family_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btnSaveFamily) {
            saveFamily();
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveFamily() {
        EditText name = findViewById(R.id.editFamilyName);
        family.setName(name.getText().toString());
        ContentValues contentValues = new ContentValues();
        //Если заполнены все поля
        if (family.getName() != null && !family.getName().equals(""))
            //Редактирование семьи
            if (family.getId() != null) {
                contentValues.put(DatabaseDescription.DBFamilies.COLUMN_NAME, family.getName());
                MainActivity.thisStatic.getContentResolver().update(
                        DatabaseDescription.DBFamilies.buildContactUri(family.getId()), contentValues, null, null);
                MainActivity.dataBaseEntityService.removeFamilyRelativesByFamilyId(family.getId());
                family.getRelatives().forEach(v -> {
                    ContentValues values = new ContentValues();
                    values.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_UUID, UUID.randomUUID().toString());
                    values.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_FAMILY, family.getId());
                    values.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_RELATIVE, v.getId());
                    MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBFamiliesRelatives.CONTENT_URI, values);
                });

                MainActivity.dataBaseEntityService.setAccountModifiedCurrent();
                MainActivity.dataBaseEntityService.loadCurrentAccount();
                MainActivity.restRequestsService.checkUpdateAccount();
                FamilyAndLocationsFragment.familiesArrayAdapter.notifyDataSetChanged();
                onSupportNavigateUp();
            } //Создание новой семьи
            else {
                contentValues.put(DatabaseDescription.DBFamilies.COLUMN_ID_UUID, UUID.randomUUID().toString());
                contentValues.put(DatabaseDescription.DBFamilies.COLUMN_NAME, family.getName());
                Uri newFamily = MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBFamilies.CONTENT_URI, contentValues);
                family.getRelatives().forEach(v -> {
                    ContentValues values = new ContentValues();
                    values.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_UUID, UUID.randomUUID().toString());
                    values.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_FAMILY, Integer.valueOf(newFamily.getLastPathSegment()));
                    values.put(DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_RELATIVE, v.getId());
                    MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBFamiliesRelatives.CONTENT_URI, values);
                });
                MainActivity.dataBaseEntityService.setAccountModifiedCurrent();
                MainActivity.dataBaseEntityService.loadCurrentAccount();
                MainActivity.restRequestsService.checkUpdateAccount();
                FamilyAndLocationsFragment.familiesArrayAdapter.notifyDataSetChanged();
                onSupportNavigateUp();
            }
    }

    public void addRelatives(List<Integer> ids) {
        ids.forEach(v -> {
            Relative relative = MainActivity.dataBaseEntityService.getRelative(v);
            if (relative.getId() != null) {
                boolean flag = family.getRelatives().stream().noneMatch(m -> m.getId().equals(v));
                if (flag) {
                    family.getRelatives().add(new Relative(relative.getId(), relative.getIdUUID(),
                            relative.getEmail(), relative.getName(), relative.isPermission()));
                }
            }
        });
        familyRelativesArrayAdapter.notifyDataSetChanged();
    }
}