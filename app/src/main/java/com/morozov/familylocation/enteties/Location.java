package com.morozov.familylocation.enteties;

import com.morozov.familylocation.enums.LocationType;

import java.util.List;
import java.util.UUID;

public class Location {
    private Integer id;
    private UUID idUUID;
    private String name;
    private LocationType type;
    private Double radius;
    private List<Point> points;

    public Location() {
    }

    public Location(Integer id, UUID idUUID, String name, List<Point> points) {
        this.id = id;
        this.idUUID = idUUID;
        this.name = name;
        this.points = points;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getIdUUID() {
        return idUUID;
    }

    public void setIdUUID(UUID idUUID) {
        this.idUUID = idUUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocationType getType() {
        return type;
    }

    public void setType(LocationType type) {
        this.type = type;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
}
