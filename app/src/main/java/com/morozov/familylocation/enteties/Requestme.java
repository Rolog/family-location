package com.morozov.familylocation.enteties;

import com.morozov.familylocation.Server.MDBRequestme;

import java.util.UUID;

public class Requestme {
    private Integer id;
    private UUID idUUID;
    private UUID idWho;
    private String emailWho;
    private String emailRequest;
    private boolean permission;
    private Long modified;

    public Requestme() {
    }

    public Requestme(Integer id, UUID idUUID, UUID idWho, String emailWho, String emailRequest, boolean permission, Long modified) {
        this.id = id;
        this.idUUID = idUUID;
        this.idWho = idWho;
        this.emailWho = emailWho;
        this.emailRequest = emailRequest;
        this.permission = permission;
        this.modified = modified;
    }

    public Requestme(MDBRequestme mdbRequestme) {
        this.setIdUUID(mdbRequestme.getId());
        this.setIdWho(mdbRequestme.getIdWho());
        this.setEmailWho(mdbRequestme.getEmailWho());
        this.setEmailRequest(mdbRequestme.getEmailRequest());
        this.setPermission(mdbRequestme.isPermission());
        this.setModified(mdbRequestme.getModified());
    }

    public UUID getIdUUID() {
        return idUUID;
    }

    public void setIdUUID(UUID idUUID) {
        this.idUUID = idUUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getIdWho() {
        return idWho;
    }

    public void setIdWho(UUID idWho) {
        this.idWho = idWho;
    }

    public String getEmailWho() {
        return emailWho;
    }

    public void setEmailWho(String emailWho) {
        this.emailWho = emailWho;
    }

    public String getEmailRequest() {
        return emailRequest;
    }

    public void setEmailRequest(String emailRequest) {
        this.emailRequest = emailRequest;
    }

    public boolean isPermission() {
        return permission;
    }

    public void setPermission(boolean permission) {
        this.permission = permission;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }
}
