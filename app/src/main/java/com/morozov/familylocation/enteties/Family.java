package com.morozov.familylocation.enteties;

import com.morozov.familylocation.Server.MDBRelative;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Family {
    private Integer id;
    private UUID idUUID;
    private String name;
    private List<Relative> relatives;

    public Family() { }

    public Family(Integer id, UUID idUUID, String name, List<Relative> relatives) {
        this.id = id;
        this.idUUID = idUUID;
        this.name = name;
        this.relatives = relatives;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getIdUUID() {
        return idUUID;
    }

    public void setIdUUID(UUID idUUID) {
        this.idUUID = idUUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Relative> relatives) {
        this.relatives = relatives;
    }

    public void setRelativesByMDB(List<MDBRelative> relatives) {
        this.relatives = new ArrayList<>();
        relatives.forEach(v -> {
            Relative relative = new Relative();
            relative.setIdUUID(v.getId());
            relative.setEmail(v.getEmail());
            relative.setName(v.getName());
            this.relatives.add(relative);
        });
    }
}
