package com.morozov.familylocation.enteties;

public class Setting {
    private Integer id;
    private String protocol;
    private String url;
    private Long port;
    private Long timeUpdate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getPort() {
        return port;
    }

    public void setPort(Long port) {
        this.port = port;
    }

    public Long getTimeUpdate() {
        return timeUpdate;
    }

    public void setTimeUpdate(Long timeUpdate) {
        this.timeUpdate = timeUpdate;
    }
}
