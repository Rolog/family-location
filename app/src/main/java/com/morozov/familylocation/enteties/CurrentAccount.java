package com.morozov.familylocation.enteties;

import com.morozov.familylocation.Server.MDBAccount;
import com.morozov.familylocation.Server.MDBFamily;
import com.morozov.familylocation.Server.MDBLocation;
import com.morozov.familylocation.Server.MDBRelative;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CurrentAccount {
    private Integer id;
    private UUID idUUID;
    private String email;
    private String password;
    private List<Family> families;
    private List<Relative> relatives;
    private List<Location> locations;
    private Long modified;

    public CurrentAccount() {
    }

    public CurrentAccount(MDBAccount account) {
        this.setIdUUID(account.getId());
        this.setEmail(account.getEmail());
        this.setPassword(account.getPassword());
        this.setFamiliesByMDB(account.getFamilies());
        this.setRelativesByMDB(account.getRelatives());
        this.setLocationsByMDB(account.getLocations());
        this.setModified(account.getModified());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getIdUUID() {
        return idUUID;
    }

    public void setIdUUID(UUID idUUID) {
        this.idUUID = idUUID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Family> getFamilies() {
        return families;
    }

    public void setFamilies(List<Family> families) {
        this.families = families;
    }

    public void setFamiliesByMDB(List<MDBFamily> families) {
        this.families = new ArrayList<>();
        families.forEach(v -> {
            Family family = new Family();
            family.setIdUUID(v.getId());
            family.setName(v.getName());
            family.setRelativesByMDB(v.getRelatives());
            this.families.add(family);
        });
    }

    public List<Relative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<Relative> relatives) {
        this.relatives = relatives;
    }

    public void setRelativesByMDB(List<MDBRelative> relatives) {
        this.relatives = new ArrayList<>();
        relatives.forEach(v -> {
            Relative relative = new Relative();
            relative.setIdUUID(v.getId());
            relative.setEmail(v.getEmail());
            relative.setName(v.getName());
            this.relatives.add(relative);
        });
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public void setLocationsByMDB(List<MDBLocation> locations) {
        this.locations = new ArrayList<>();
        locations.forEach(v -> {
            Location location = new Location();
            location.setIdUUID(v.getId());
            location.setName(v.getName());
            location.setPoints(v.getPoints());
            location.setType(v.getType());
            location.setRadius(v.getRadius());
            this.locations.add(location);
        });
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }
}
