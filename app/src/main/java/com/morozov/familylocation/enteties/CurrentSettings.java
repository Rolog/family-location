package com.morozov.familylocation.enteties;

public class CurrentSettings {
    //TODO: Добавить настройки подключения к БД

    //Частота отправки данных
    private Long timeUpdateSend;
    //Частота получения данных
    private Long timeUpdateReceive;

    public Long getTimeUpdateSend() {
        return timeUpdateSend;
    }

    public void setTimeUpdateSend(Long timeUpdateSend) {
        this.timeUpdateSend = timeUpdateSend;
    }

    public Long getTimeUpdateReceive() {
        return timeUpdateReceive;
    }

    public void setTimeUpdateReceive(Long timeUpdateReceive) {
        this.timeUpdateReceive = timeUpdateReceive;
    }
}
