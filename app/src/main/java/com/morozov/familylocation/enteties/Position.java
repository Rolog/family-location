package com.morozov.familylocation.enteties;

import com.morozov.familylocation.Server.MDBPosition;

import java.util.UUID;

public class Position {
    private Integer id;
    private UUID idUUID;
    private UUID accountId;
    private String accountEmail;
    private Double lat;
    private Double lon;
    private Double altitude;
    private Double speed;
    private Long modified;

    public Position() {
    }

    public Position(MDBPosition mdbPosition) {
        this.setIdUUID(mdbPosition.getId());
        this.setAccountId(mdbPosition.getAccountId());
        this.setAccountEmail(mdbPosition.getAccountEmail());
        this.setLat(mdbPosition.getLat());
        this.setLon(mdbPosition.getLon());
        this.setAltitude(mdbPosition.getAltitude());
        this.setSpeed(mdbPosition.getSpeed());
        this.setModified(mdbPosition.getModified());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getIdUUID() {
        return idUUID;
    }

    public void setIdUUID(UUID idUUID) {
        this.idUUID = idUUID;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }
}
