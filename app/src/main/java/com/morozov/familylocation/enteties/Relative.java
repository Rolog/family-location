package com.morozov.familylocation.enteties;

import java.util.UUID;

public class Relative {
    private Integer id;
    private UUID idUUID;
    private String email;
    private String name;
    private boolean isPermission;

    public Relative() {   }

    public Relative(Integer id, UUID idUUID, String email, String name, boolean isPermission) {
        this.id = id;
        this.idUUID = idUUID;
        this.email = email;
        this.name = name;
        this.isPermission = isPermission;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UUID getIdUUID() {
        return idUUID;
    }

    public void setIdUUID(UUID idUUID) {
        this.idUUID = idUUID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPermission() {
        return isPermission;
    }

    public void setPermission(boolean permission) {
        isPermission = permission;
    }
}
