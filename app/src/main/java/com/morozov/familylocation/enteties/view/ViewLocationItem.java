package com.morozov.familylocation.enteties.view;

import com.morozov.familylocation.enteties.Relative;

public class ViewLocationItem {
    private Relative relative;
    private String locationName;

    public Relative getRelative() {
        return relative;
    }

    public void setRelative(Relative relative) {
        this.relative = relative;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
