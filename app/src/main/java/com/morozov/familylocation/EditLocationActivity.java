package com.morozov.familylocation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Location;
import com.morozov.familylocation.enteties.Point;
import com.morozov.familylocation.ui.familyandlocations.FamilyAndLocationsFragment;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polygon;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class EditLocationActivity extends AppCompatActivity {
    MapView map = null;
    private MyLocationNewOverlay mLocationOverlay;
    private List<GeoPoint> geoPoints = new ArrayList<>();
    private Polygon polygon = new Polygon();
    public Boolean isCheckPoints = false;
    private Marker startMarker;
    private Location location = new Location();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_edit_location);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_edit_location);

        map = (MapView) findViewById(R.id.mapEditLocation);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT);
        map.setMultiTouchControls(true);

        IMapController mapController = map.getController();
        mapController.setZoom(19.);

        this.mLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(this), map);
        this.mLocationOverlay.enableMyLocation();
        map.getOverlays().add(this.mLocationOverlay);

        final MapEventsReceiver mReceive = new MapEventsReceiver(){
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                if (isCheckPoints) {
                    if (geoPoints.size() == 0) {
                        geoPoints.add(p);
                        geoPoints.add(geoPoints.get(0));
                        addMarker();
                        startMarker.setPosition(geoPoints.get(0));
                    } else
                        geoPoints.add(geoPoints.size()-1, p);
                }
                polygon.setPoints(geoPoints);
                map.invalidate();
                return false;
            }
            @Override
            public boolean longPressHelper(GeoPoint p) {
                return false;
            }
        };
        map.getOverlays().add(new MapEventsOverlay(mReceive));

        polygon.setFillColor(Color.argb(75, 255,0,0));
        //polygon.setTitle("A sample polygon");

        map.getOverlayManager().add(polygon);

        startMarker = new Marker(map);
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        startMarker.setInfoWindow(null);
        startMarker.setTitle(this.getResources().getString(R.string.location_start_point));
        addMarker();

        FloatingActionButton fbtn = findViewById(R.id.fbtnCheckPoints);
        fbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isCheckPoints = !isCheckPoints;
                btnCheckPointsSetBackgroundTint();
                if (isCheckPoints)
                    map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.NEVER);
                else
                    map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT);
            }
        });

        FloatingActionButton fbtnClear = findViewById(R.id.fbtnClearCheckPoints);
        fbtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                geoPoints.clear();
                removeMarker();
                polygon.setPoints(geoPoints);
                map.invalidate();
            }
        });

        location.setPoints(new ArrayList<>());
        try {
            Bundle b = getIntent().getExtras();
            int id = b.getInt("id");
            location = MainActivity.dataBaseEntityService.getLocation(id);
        } catch (Exception ignored) {}
        EditText name = findViewById(R.id.editEditLocationName);
        if (location.getName() != null)
            name.setText(location.getName());
        location.getPoints().forEach(v -> {
            geoPoints.add(new GeoPoint(v.getLat(), v.getLon()));
        });
        if (geoPoints.size() > 0) {
            polygon.setPoints(geoPoints);
            mapController.setCenter(geoPoints.get(0));
            addMarker();
            startMarker.setPosition(geoPoints.get(0));
            map.invalidate();
        }
        else {
            removeMarker();
            mapController.setCenter(new GeoPoint(MainActivity.currentGeoPoint.getLatitude(), MainActivity.currentGeoPoint.getLongitude()));
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onResume(){
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause(){
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.edit_location_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btnSaveEditLocation) {
            saveLocation();
        }
        return super.onOptionsItemSelected(item);
    }

    public void btnCheckPointsSetBackgroundTint() {
        FloatingActionButton button = findViewById(R.id.fbtnCheckPoints);
        if (isCheckPoints)
            button.setBackgroundTintList(ColorStateList.valueOf(this.getColor(R.color.location_button_check)));
        else
            button.setBackgroundTintList(ColorStateList.valueOf(this.getColor(R.color.location_button_uncheck)));
    }

    public void saveLocation() {
        EditText name = findViewById(R.id.editEditLocationName);
        location.setName(name.getText().toString());
        location.setPoints(new ArrayList<>());
        final Integer[] i = {0};
        geoPoints.forEach(v -> {
            location.getPoints().add(new Point(i[0], location.getId(), v.getLatitude(), v.getLongitude()));
            i[0]++;
        });
        ContentValues contentValues = new ContentValues();
        //Если заполнены все поля
        if (location.getName() != null && !location.getName().equals(""))
            //Редактирование локации
            if (location.getId() != null) {
                contentValues.put(DatabaseDescription.DBLocations.COLUMN_NAME, location.getName());
                MainActivity.thisStatic.getContentResolver().update(
                        DatabaseDescription.DBLocations.buildContactUri(location.getId()), contentValues, null, null);
                MainActivity.dataBaseEntityService.removePointsByLocationId(location.getId());
                location.getPoints().forEach(v -> {
                    ContentValues values = new ContentValues();
                    values.put(DatabaseDescription.DBPoints.COLUMN_NUMBER, v.getNumber());
                    values.put(DatabaseDescription.DBPoints.COLUMN_ID_LOCATION, location.getId());
                    values.put(DatabaseDescription.DBPoints.COLUMN_LAT, v.getLat());
                    values.put(DatabaseDescription.DBPoints.COLUMN_LON, v.getLon());
                    MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBPoints.CONTENT_URI, values);
                });

                MainActivity.dataBaseEntityService.setAccountModifiedCurrent();
                MainActivity.dataBaseEntityService.loadCurrentAccount();
                MainActivity.restRequestsService.checkUpdateAccount();
                FamilyAndLocationsFragment.editLocationsArrayAdapter.notifyDataSetChanged();
                onSupportNavigateUp();
            } //Создание новой локации
            else {
                contentValues.put(DatabaseDescription.DBLocations.COLUMN_ID_UUID, UUID.randomUUID().toString());
                contentValues.put(DatabaseDescription.DBLocations.COLUMN_NAME, location.getName());
                Uri newLocation = MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBLocations.CONTENT_URI, contentValues);
                location.getPoints().forEach(v -> {
                    ContentValues values = new ContentValues();
                    values.put(DatabaseDescription.DBPoints.COLUMN_NUMBER, v.getNumber());
                    values.put(DatabaseDescription.DBPoints.COLUMN_ID_LOCATION, Integer.valueOf(newLocation.getLastPathSegment()));
                    values.put(DatabaseDescription.DBPoints.COLUMN_LAT, v.getLat());
                    values.put(DatabaseDescription.DBPoints.COLUMN_LON, v.getLon());
                    MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBPoints.CONTENT_URI, values);
                });
                MainActivity.dataBaseEntityService.setAccountModifiedCurrent();
                MainActivity.dataBaseEntityService.loadCurrentAccount();
                MainActivity.restRequestsService.checkUpdateAccount();
                FamilyAndLocationsFragment.editLocationsArrayAdapter.notifyDataSetChanged();
                onSupportNavigateUp();
            }
    }

    public void addMarker() {
        map.getOverlays().add(startMarker);
        map.invalidate();
    }

    public void removeMarker() {
        map.getOverlays().remove(startMarker);
        map.invalidate();
    }

}