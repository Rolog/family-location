package com.morozov.familylocation.Server;

import com.morozov.familylocation.enteties.Position;

import java.util.UUID;

public class MDBPosition {
    private UUID id;
    private UUID accountId;
    private String accountEmail;
    private Double lat;
    private Double lon;
    private Double altitude;
    private Double speed;
    private Long modified;

    public MDBPosition() {
    }

    public MDBPosition(Position position) {
        this.setId(position.getIdUUID());
        this.setAccountId(position.getAccountId());
        this.setAccountEmail(position.getAccountEmail());
        this.setLat(position.getLat());
        this.setLon(position.getLon());
        this.setAltitude(position.getAltitude());
        this.setSpeed(position.getSpeed());
        this.setModified(position.getModified());
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccountId() {
        return accountId;
    }

    public void setAccountId(UUID accountId) {
        this.accountId = accountId;
    }

    public String getAccountEmail() {
        return accountEmail;
    }

    public void setAccountEmail(String accountEmail) {
        this.accountEmail = accountEmail;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }
}
