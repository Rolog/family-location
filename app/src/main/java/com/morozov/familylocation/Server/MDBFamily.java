package com.morozov.familylocation.Server;

import com.morozov.familylocation.enteties.Relative;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MDBFamily {
    private UUID id;
    private String name;
    private List<MDBRelative> relatives;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MDBRelative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<MDBRelative> relatives) {
        this.relatives = relatives;
    }

    public void setRelativesByAND(List<Relative> relatives) {
        this.relatives = new ArrayList<>();
        relatives.forEach(v -> {
            MDBRelative mdbRelative = new MDBRelative();
            mdbRelative.setId(v.getIdUUID());
            mdbRelative.setEmail(v.getEmail());
            mdbRelative.setName(v.getName());
            this.relatives.add(mdbRelative);
        });
    }
}
