package com.morozov.familylocation.Server;

import com.morozov.familylocation.enteties.CurrentAccount;
import com.morozov.familylocation.enteties.Family;
import com.morozov.familylocation.enteties.Location;
import com.morozov.familylocation.enteties.Relative;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MDBAccount {
    private UUID id;
    private String email;
    private String password;
    private List<MDBFamily> families;
    private List<MDBRelative> relatives;
    private List<MDBLocation> locations;
    private Long modified;

    public MDBAccount() {
    }

    public MDBAccount(CurrentAccount account) {
        this.setId(account.getIdUUID());
        this.setEmail(account.getEmail());
        this.setPassword(account.getPassword());
        this.setFamiliesByAND(account.getFamilies());
        this.setRelativesByAND(account.getRelatives());
        this.setLocationsByAND(account.getLocations());
        this.setModified(account.getModified());
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<MDBFamily> getFamilies() {
        return families;
    }

    public void setFamilies(List<MDBFamily> families) {
        this.families = families;
    }

    public void setFamiliesByAND(List<Family> families) {
        this.families = new ArrayList<>();
        families.forEach(v -> {
            MDBFamily mdbFamily = new MDBFamily();
            mdbFamily.setId(v.getIdUUID());
            mdbFamily.setName(v.getName());
            mdbFamily.setRelativesByAND(v.getRelatives());
            this.families.add(mdbFamily);
        });
    }

    public List<MDBRelative> getRelatives() {
        return relatives;
    }

    public void setRelatives(List<MDBRelative> MDBRelatives) {
        this.relatives = MDBRelatives;
    }

    public void setRelativesByAND(List<Relative> relatives) {
        this.relatives = new ArrayList<>();
        relatives.forEach(v -> {
            MDBRelative mdbRelative = new MDBRelative();
            mdbRelative.setId(v.getIdUUID());
            mdbRelative.setEmail(v.getEmail());
            mdbRelative.setName(v.getName());
            this.relatives.add(mdbRelative);
        });
    }

    public List<MDBLocation> getLocations() {
        return locations;
    }

    public void setLocations(List<MDBLocation> MDBLocations) {
        this.locations = MDBLocations;
    }

    public void setLocationsByAND(List<Location> locations) {
        this.locations = new ArrayList<>();
        locations.forEach(v -> {
            MDBLocation mdbLocation = new MDBLocation();
            mdbLocation.setId(v.getIdUUID());
            mdbLocation.setName(v.getName());
            mdbLocation.setType(v.getType());
            mdbLocation.setRadius(v.getRadius());
            mdbLocation.setPoints(v.getPoints());
            this.locations.add(mdbLocation);
        });
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }
}
