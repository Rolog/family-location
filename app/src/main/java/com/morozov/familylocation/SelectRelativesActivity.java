package com.morozov.familylocation;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.morozov.familylocation.Utils.Constants;
import com.morozov.familylocation.Utils.SelectRelativesArrayAdapter;
import com.morozov.familylocation.enteties.Relative;

import java.util.List;
import java.util.Objects;

public class SelectRelativesActivity extends AppCompatActivity {
    private SelectRelativesArrayAdapter selectRelativesArrayAdapter;
    private ListView relativesListView; // Для вывода информации

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_relatives);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_select_relatives);

        List<Relative> relatives = MainActivity.dataBaseEntityService.getAllRelatives();
        relativesListView = this.findViewById(R.id.listSelectRelatives);
        selectRelativesArrayAdapter = new SelectRelativesArrayAdapter(this, relatives);
        relativesListView.setAdapter(selectRelativesArrayAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.select_relatives_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.btnSelectRelatives) {
            selected();
        } else
            if (id == R.id.btnSelectAllRelatives) {
                selectAll();
            }
        return super.onOptionsItemSelected(item);
    }

    public void selectAll() {
        CheckBox checkBox;
        for (int i = 0; i < relativesListView.getCount(); i++) {
            checkBox = relativesListView.getChildAt(i).findViewById(R.id.checkBoxSelectRelatives);
            checkBox.setChecked(true);
        }
    }

    public void selected() {
        CheckBox checkBox;
        Integer count = 0;
        Intent intent = new Intent();
        for (int i = 0; i < relativesListView.getCount(); i++) {
            checkBox = relativesListView.getChildAt(i).findViewById(R.id.checkBoxSelectRelatives);
            if (checkBox.isChecked()) {
                intent.putExtra("id_" + count, Integer.valueOf(((TextView) relativesListView.getChildAt(i).findViewById(R.id.textIdSelectRelatives)).getText().toString()));
                count++;
            }
        }
        intent.putExtra("count", count);
        setResult(Constants.RESULT_OK, intent);
        finish();
    }
}