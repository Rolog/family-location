package com.morozov.familylocation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.morozov.familylocation.Server.MDBPosition;
import com.morozov.familylocation.Utils.LocationFamilyArrayAdapter;
import com.morozov.familylocation.enteties.Position;
import com.morozov.familylocation.enteties.RelativeInLocation;
import com.morozov.familylocation.ui.location.LocationFragment;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.CustomZoomButtonsController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.simplefastpoint.LabelledGeoPoint;
import org.osmdroid.views.overlay.simplefastpoint.SimpleFastPointOverlay;
import org.osmdroid.views.overlay.simplefastpoint.SimpleFastPointOverlayOptions;
import org.osmdroid.views.overlay.simplefastpoint.SimplePointTheme;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.IntStream;

public class PathOnMapActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @SuppressLint("StaticFieldLeak")
    public static EditText startTime;
    @SuppressLint("StaticFieldLeak")
    public static EditText endTime;
    public static Integer timeControl;
    public static Long start;
    public static Long end;
    public static MapView map = null;
    public static TextView textViewNoData = null;
    private static Polyline polyline = new Polyline();
    public static Spinner spinner;
    public static IMapController mapController;
    public static SimpleFastPointOverlay sfpo = new SimpleFastPointOverlay(new SimplePointTheme(new ArrayList<>()));
    private static Marker startMarker;
    private static Marker endMarker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        setContentView(R.layout.activity_path_on_map);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_path_on_map);

        textViewNoData = findViewById(R.id.textPathOnMap);
        map = (MapView) findViewById(R.id.mapPathOnMap);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.getZoomController().setVisibility(CustomZoomButtonsController.Visibility.SHOW_AND_FADEOUT);
        map.setMultiTouchControls(true);

        mapController = map.getController();
        mapController.setZoom(19.);

        startTime = (EditText) findViewById(R.id.editPathOnMapStart);
        startTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeControl = 0;
                showTimePickerDialog(v);
                showDatePickerDialog(v);
            }
        });

        endTime = (EditText) findViewById(R.id.editPathOnMapEnd);
        endTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timeControl = 1;
                showTimePickerDialog(v);
                showDatePickerDialog(v);
            }
        });

        polyline = new Polyline();
        polyline.setColor(Color.argb(75, 0,0,255));
        polyline.setWidth(5);
        map.getOverlays().add(polyline);

        List<String> relativeNames = new ArrayList<>();
        MainActivity.currentAccount.getRelatives().forEach(v -> {
            relativeNames.add(v.getName());
        });
        spinner = findViewById(R.id.spinnerPathOnMapSelectRelative);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, relativeNames.toArray(new String[0]));

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        map.getOverlays().add(sfpo);

        startMarker = new Marker(map);
        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        startMarker.setVisible(false);
        map.getOverlays().add(startMarker);
        endMarker = new Marker(map);
        endMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        endMarker.setVisible(false);
        map.getOverlays().add(endMarker);

        try {
            Bundle b = getIntent().getExtras();
            String bid = b.getString("id");
            String bname = b.getString("name");
            Long bstart  = b.getLong("start");
            Long bend  = b.getLong("end");
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            if (bstart != null) {
                start = bstart;
                startTime.setText(formatter.format(new Date(start)));
            }
            if (bend != null) {
                end = bend;
                endTime.setText(formatter.format(new Date(end)));
            }
            if (bid != null) {
                spinner.setSelection(IntStream.range(0, MainActivity.currentAccount.getRelatives().size())
                        .filter(ind-> MainActivity.currentAccount.getRelatives().get(ind).getIdUUID().equals(UUID.fromString(bid)))
                        .findFirst()
                        .getAsInt());
            }
        } catch (Exception ignored) {}

        //refreshPathOnMap();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        refreshPathOnMap();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    //region DatePicker
    @SuppressLint("SimpleDateFormat")
    public static void setDate(String value) {
        SimpleDateFormat formatter;
        if (value.length() > 10)
            formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        else
            formatter = new SimpleDateFormat("dd.MM.yyyy");
        if (timeControl == 0) {
            startTime.setText(value);
            try {
                start = Objects.requireNonNull(formatter.parse(startTime.getText().toString())).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            endTime.setText(value);
            try {
                end = Objects.requireNonNull(formatter.parse(endTime.getText().toString())).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (value.length() > 10)
            refreshPathOnMap();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String text = (day < 10 ? "0" + day : day) + "." + ((month + 1) < 10 ? "0" + (month + 1) : (month + 1)) + "." + year;
            setDate(text);
        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment implements
            TimePickerDialog.OnTimeSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        @SuppressLint("SetTextI18n")
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            if (timeControl == 0) {
                setDate((startTime.getText().length() > 10 ? startTime.getText().toString().substring(0, 10) : startTime.getText()) + " " + (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" +
                        (minute < 10 ? "0" + minute : minute));
            }
            else
                setDate((endTime.getText().length() > 10 ? endTime.getText().toString().substring(0, 10) : endTime.getText()) + " " + (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":"	+
                        (minute < 10 ? "0" + minute : minute));
        }
    }
    //endregion

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return true;
    }

    public static void refreshPathOnMap() {
        if (MainActivity.currentAccount.getRelatives().size() > 0 && start != null && end != null)
            MainActivity.restRequestsService.getRelativePath(MainActivity.currentAccount.getRelatives().get(spinner.getSelectedItemPosition()).getIdUUID(), start, end);
    }

    public static void drawPathOnMap(List<MDBPosition> positions) {
        List<GeoPoint> geoPoints = new ArrayList<>();
        List<IGeoPoint> geoPointsI = new ArrayList<>();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm:ss");
        positions.forEach(v -> {
            geoPoints.add(new GeoPoint(v.getLat(), v.getLon()));
            geoPointsI.add(new LabelledGeoPoint(v.getLat(), v.getLon(), simpleDateFormat.format(v.getModified())));
        });
        map.getOverlays().remove(sfpo);
        polyline.setPoints(geoPoints);
        startMarker.setVisible(false);
        endMarker.setVisible(false);

        if (geoPoints.size() > 0) {
            textViewNoData.setVisibility(View.GONE);
            startMarker.setTitle(MainActivity.thisStatic.getString(R.string.path_on_map_start));
            startMarker.setPosition(geoPoints.get(0));
            startMarker.setVisible(true);
            if (geoPoints.size() > 1) {
                endMarker.setTitle(MainActivity.thisStatic.getString(R.string.path_on_map_end));
                endMarker.setPosition(geoPoints.get(geoPoints.size()-1));
                endMarker.setVisible(true);
            }
            mapController.setCenter(geoPoints.get(0));

            Paint textStyle = new Paint();
            textStyle.setStyle(Paint.Style.FILL);
            textStyle.setColor(Color.parseColor("#0000ff"));
            textStyle.setTextAlign(Paint.Align.CENTER);
            textStyle.setTextSize(30);
            SimpleFastPointOverlayOptions opt = SimpleFastPointOverlayOptions.getDefaultStyle()
                    .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.MAXIMUM_OPTIMIZATION)
                    .setRadius(7).setIsClickable(true).setCellSize(15).setTextStyle(textStyle);

            sfpo = new SimpleFastPointOverlay(new SimplePointTheme(geoPointsI, true), opt);
            map.getOverlays().add(sfpo);
        } else {
            textViewNoData.setVisibility(View.VISIBLE);
        }
        map.invalidate();
    }
}