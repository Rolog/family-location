package com.morozov.familylocation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.morozov.familylocation.AsyncTask.RestRequestsService;
import com.morozov.familylocation.GPSService.GPSReceiver;
import com.morozov.familylocation.GPSService.GPSService;
import com.morozov.familylocation.Utils.Constants;
import com.morozov.familylocation.data.DataBaseEntityService;
import com.morozov.familylocation.data.LoginDataSource;
import com.morozov.familylocation.data.LoginRepository;
import com.morozov.familylocation.databinding.ActivityMainBinding;
import com.morozov.familylocation.enteties.CurrentAccount;
import com.morozov.familylocation.enteties.Position;
import com.morozov.familylocation.enteties.Requestme;
import com.morozov.familylocation.enteties.Setting;
import com.morozov.familylocation.ui.location.LocationFragment;
import com.morozov.familylocation.ui.login.LoginActivity;
import com.morozov.familylocation.update.CheckForUpdate;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;

    public NavController navController;
    public static TextView navLat;
    public static TextView navLon;

    public static CurrentAccount currentAccount = new CurrentAccount();
    public static List<Requestme> requestsmes = new ArrayList<>();
    public static Setting settings = new Setting();
    public static DataBaseEntityService dataBaseEntityService = new DataBaseEntityService();
    public static RestRequestsService restRequestsService = new RestRequestsService();
    @SuppressLint("StaticFieldLeak")
    public static MainActivity thisStatic;
    public static GeoPoint currentGeoPoint = new GeoPoint(0., 0.);

    ActivityResultLauncher<Intent> activityResultLaunch;

    @SuppressLint("BatteryLife")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] permissions = checkPermission();
        if (permissions.length > 0) {
            ActivityCompat.requestPermissions(this,
                    permissions,
                    Constants.PERMISSION_REQUEST_ACCESS_FINE_LOCATION);
        } else {
            activityResultLaunch = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            recreate();
                        } else if (result.getResultCode() == Constants.RESULT_SETTINGS_OK) {
                            stopService(new Intent(MainActivity.this, GPSService.class));
                            startGPSService();
                        }
                    }
                });

            //Оптимизация батареи
            Intent intent1 = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent1.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent1.setData(Uri.parse("package:" + packageName));
                startActivity(intent1);
            }

            //installtion permission

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (!getPackageManager().canRequestPackageInstalls()) {
                    startActivity(new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES).setData(Uri.parse(String.format("package:%s", getPackageName()))));
                } else {
                }
            }

            MainActivity.thisStatic = this;
            dataBaseEntityService.initCurrentAccount();
            dataBaseEntityService.loadCurrentAccount();
            dataBaseEntityService.loadSettings();

            if (currentAccount.getId() != null) {
                binding = ActivityMainBinding.inflate(getLayoutInflater());
                setContentView(binding.getRoot());

                dataBaseEntityService.loadRequestsMe();
                restRequestsService.updateRequestsme();
                restRequestsService.checkUpdateAccount();
                setSupportActionBar(binding.appBarMenu.toolbar);
                DrawerLayout drawer = binding.drawerLayout;
                NavigationView navigationView = binding.navView;
                // Passing each menu ID as a set of Ids because each
                // menu should be considered as top level destinations.
                mAppBarConfiguration = new AppBarConfiguration.Builder(
                        R.id.nav_location, R.id.nav_account, R.id.nav_family_and_locations,
                        R.id.nav_requestsme, R.id.nav_settings)
                        .setOpenableLayout(drawer)
                        .build();
                navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_menu);
                NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
                NavigationUI.setupWithNavController(navigationView, navController);
                navigationView.setNavigationItemSelectedListener(this);

                View headerView = navigationView.getHeaderView(0);
                TextView navUsername = (TextView) headerView.findViewById(R.id.textView);
                navUsername.setText(currentAccount.getEmail());
                navLat = (TextView) headerView.findViewById(R.id.textNavHeaderLat);
                navLon = (TextView) headerView.findViewById(R.id.textNavHeaderLon);
                navLat.setText(MainActivity.thisStatic.getString(R.string.nav_lat) + String.valueOf(currentGeoPoint.getLatitude()));
                navLon.setText(MainActivity.thisStatic.getString(R.string.nav_lon) + String.valueOf(currentGeoPoint.getLongitude()));
                ImageButton btnShareMyLocation = headerView.findViewById(R.id.btnShareMyLocation);
                btnShareMyLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, MainActivity.thisStatic.getString(R.string.app_name) + " " +
                                MainActivity.thisStatic.getString(R.string.share_my_location) + ":" +
                                " N " + MainActivity.currentGeoPoint.getLatitude() + " E" + MainActivity.currentGeoPoint.getLongitude());
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent,MainActivity.thisStatic.getString(R.string.share_share)));
                    }
                });

                ImageButton btnToMapMyLocation = headerView.findViewById(R.id.btnToMapMyLocation);
                btnToMapMyLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.thisStatic, PositionOnMapActivity.class);
                        intent.putExtra("lat", MainActivity.currentGeoPoint.getLatitude());
                        intent.putExtra("lon", MainActivity.currentGeoPoint.getLongitude());
                        intent.putExtra("name", MainActivity.thisStatic.getString(R.string.share_my_location));
                        MainActivity.thisStatic.startActivity(intent);
                    }
                });

                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    //Setting up variables
                    startGPSService();
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    IntentFilter filter = new IntentFilter("com.morozov.tracker");
                    BroadcastReceiver mReceiver = new GPSReceiver();
                    registerReceiver(mReceiver, filter);
                }
                CheckForUpdate checkForUpdate = new CheckForUpdate(this, false, false);
                checkForUpdate.execute();
            } else {
                Intent intent = new Intent(this, LoginActivity.class);
                activityResultLaunch.launch(intent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_menu);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public static void SendToInternet(double latitude,double longitude,double altitude, double speed) {
        currentGeoPoint = new GeoPoint(latitude, longitude);
        Position position = new Position();
        position.setIdUUID(UUID.randomUUID());
        position.setAccountId(currentAccount.getIdUUID());
        position.setAccountEmail(currentAccount.getEmail());
        position.setLat(latitude);
        position.setLon(longitude);
        position.setAltitude(altitude);
        position.setSpeed(speed);
        position.setModified(System.currentTimeMillis());
        dataBaseEntityService.savePosition(position);
        restRequestsService.exportPositionsToServer();
        restRequestsService.checkUpdateAccount();
        restRequestsService.updateRequestsme();

        Fragment f = Objects.requireNonNull(MainActivity.thisStatic.getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_menu))
                .getChildFragmentManager().getFragments().get(0);
        if(f instanceof LocationFragment)
            ((LocationFragment)f).refreshLocationFamily();

        navLat.setText(MainActivity.thisStatic.getString(R.string.nav_lat) + String.valueOf(currentGeoPoint.getLatitude()));
        navLon.setText(MainActivity.thisStatic.getString(R.string.nav_lon) + String.valueOf(currentGeoPoint.getLongitude()));
/*
        FragmentManager fm = MainActivity.thisStatic.getSupportFragmentManager();

        //if you added fragment via layout xml
        LocationFragment fragment = (LocationFragment)fm.findFragmentById(R.id.nav_location);
        fragment.();*/
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    protected void startGPSService() {
        GPSService.LocationInterval = settings.getTimeUpdate().intValue();
        GPSService.LocationFastestInterval = settings.getTimeUpdate().intValue();
        PendingIntent contentIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            contentIntent = PendingIntent.getActivity
                    (this, 0, new Intent(this, MainActivity.class), PendingIntent.FLAG_MUTABLE);
        }
        else
        {
            contentIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, MainActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        }

        GPSService.contentIntent = contentIntent;
        GPSService.NotificationTitle = this.getResources().getString(R.string.app_is_tracking_you);
        GPSService.NotificationTxt = this.getResources().getString(R.string.app_name);
        GPSService.drawable_small = R.mipmap.ic_launcher;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(MainActivity.this, GPSService.class));
        } else {

            startService(new Intent(MainActivity.this, GPSService.class));
            Toast.makeText(MainActivity.this, this.getResources().getString(R.string.tracking_started), Toast.LENGTH_SHORT).show();
        }
    }

    //region checkPermission
    protected String[] checkPermission(){
        List<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ){//Can add more as per requirement

            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ){//Can add more as per requirement
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        return permissions.toArray(new String[0]);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.PERMISSION_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    recreate();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    //endregion

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Создадим новый фрагмент
        Fragment fragment = null;
        Class fragmentClass = null;

        int id = item.getItemId();
        if (id == R.id.nav_settings) {
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            activityResultLaunch.launch(intent);
        }
        else if (id == R.id.nav_exit) {
            //loginRepository.logout();
            LoginRepository.getInstance(new LoginDataSource()).logout();
            stopService(new Intent(MainActivity.this, GPSService.class));
            navController.navigate(R.id.nav_location);
            recreate();
            //Toast.makeText(getApplicationContext(), "Exit", Toast.LENGTH_LONG).show();
        }
        else
            NavigationUI.onNavDestinationSelected(item, navController);
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        //return true;
        return super.onOptionsItemSelected(item);
    }
}