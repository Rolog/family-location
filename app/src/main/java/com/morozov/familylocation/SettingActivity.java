package com.morozov.familylocation;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.morozov.familylocation.Utils.Constants;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.ui.requestsme.RequestsMeFragment;
import com.morozov.familylocation.update.CheckForUpdate;

import java.util.Objects;

public class SettingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spinner;
    EditText editText1;
    EditText editText2;
    EditText editText3;
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    Switch aSwitch;
    public  SettingActivity thisContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(R.string.menu_settings);

        thisContext = this;

        TextView version = findViewById(R.id.tvVersionAppSettings);
        try {
            version.setText(getText(R.string.current_vesion_app) + " " + this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String[] protocols = {
                getResources().getString(R.string.select_protpcol),
                "HTTPS",
                "HTTP"
        };
        spinner = findViewById(R.id.spinnerSettingProtocol);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, protocols);

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        spinner.setEnabled(false);

        editText1 = findViewById(R.id.editSettingURL);
        editText2 = findViewById(R.id.editSettingPort);
        editText3 = findViewById(R.id.editSettingUpdateTime);

        aSwitch = findViewById(R.id.switchUserSettings);
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!aSwitch.isChecked()) {
                    setDefault();
                }
                spinner.setEnabled(aSwitch.isChecked());
                editText1.setEnabled(aSwitch.isChecked());
                editText2.setEnabled(aSwitch.isChecked());
                editText3.setEnabled(aSwitch.isChecked());

            }
        });

        Button btnReset = findViewById(R.id.btnResetSettings);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBDeleteSettings.CONTENT_URI, null, null);
                MainActivity.dataBaseEntityService.loadSettings();
                setDefault();
                setResult(Constants.RESULT_SETTINGS_OK);
            }
        });

        Button btnTestConnect = findViewById(R.id.btnTestConnect);
        btnTestConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MainActivity.restRequestsService.testConnect())
                    Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.test_connect_ok), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.test_connect_fail), Toast.LENGTH_SHORT).show();
            }
        });

        Button btnUpdate = findViewById(R.id.btnUpdateAppSettings);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckForUpdate checkForUpdate = new CheckForUpdate(thisContext, true, true);
                checkForUpdate.execute();
            }
        });

        setDefault();
    }

    void setDefault() {
        spinner.setSelection(MainActivity.settings.getProtocol().equals("HTTPS") ? 1 :
                MainActivity.settings.getProtocol().equals("HTTP") ? 2 : 0);
        editText1.setText(MainActivity.settings.getUrl());
        if (MainActivity.settings.getPort() == null)
            editText2.setText("");
        else
            editText2.setText(MainActivity.settings.getPort().toString());
        if (MainActivity.settings.getTimeUpdate() == null)
            editText3.setText("");
        else
            editText3.setText(MainActivity.settings.getTimeUpdate().toString());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // R.menu.mymenu is a reference to an xml file named mymenu.xml which should be inside your res/menu directory.
        // If you don't have res/menu, just create a directory named "menu" inside res
        getMenuInflater().inflate(R.menu.setting_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btnSaveSetting) {
            saveSettings();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    void saveSettings() {
        if (aSwitch.isChecked()) {
            ContentValues values = new ContentValues();
            if (spinner.getSelectedItemPosition() > 0)
                values.put(DatabaseDescription.DBSettings.COLUMN_PROTOCOL, spinner.getSelectedItem().toString());
            else
                values.putNull(DatabaseDescription.DBSettings.COLUMN_PROTOCOL);
            values.put(DatabaseDescription.DBSettings.COLUMN_URL, editText1.getText().toString());
            if (!editText2.getText().toString().equals("") && Long.parseLong(editText2.getText().toString()) > 0)
                values.put(DatabaseDescription.DBSettings.COLUMN_PORT, Long.valueOf(editText2.getText().toString()));
            else
                values.putNull(DatabaseDescription.DBSettings.COLUMN_PORT);
            if (!editText3.getText().toString().equals("") && Long.parseLong(editText3.getText().toString()) > 0)
                values.put(DatabaseDescription.DBSettings.COLUMN_TIME_UPDATE, Long.valueOf(editText3.getText().toString()));
            else
                values.putNull(DatabaseDescription.DBSettings.COLUMN_TIME_UPDATE);
            if (MainActivity.settings.getId() != null)
                MainActivity.thisStatic.getContentResolver().update(DatabaseDescription.DBSettings.buildContactUri(MainActivity.settings.getId()), values, null, null);
            else
                MainActivity.thisStatic.getContentResolver().insert(DatabaseDescription.DBSettings.CONTENT_URI, values);
            MainActivity.dataBaseEntityService.loadSettings();
            setResult(Constants.RESULT_SETTINGS_OK);
            Toast.makeText(MainActivity.thisStatic, MainActivity.thisStatic.getString(R.string.settings_save_ok), Toast.LENGTH_SHORT).show();
        }
    }
}