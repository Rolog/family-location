package com.morozov.familylocation.enums;

public enum LocationType {
    //Прямоугольник
    RECTANGLE,

    //Круг
    CIRCLE,

    //Многоугольник
    POLYGON

}
