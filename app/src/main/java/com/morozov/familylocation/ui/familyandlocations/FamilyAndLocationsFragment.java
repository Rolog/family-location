package com.morozov.familylocation.ui.familyandlocations;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.tabs.TabLayout;
import com.morozov.familylocation.EditLocationActivity;
import com.morozov.familylocation.FamilyActivity;
import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.RelativeActivity;
import com.morozov.familylocation.Utils.EditLocationsArrayAdapter;
import com.morozov.familylocation.Utils.FamiliesArrayAdapter;
import com.morozov.familylocation.Utils.RelativesArrayAdapter;
import com.morozov.familylocation.databinding.FragmentFamilyAndLocationsBinding;

public class FamilyAndLocationsFragment extends Fragment {

    private FragmentFamilyAndLocationsBinding binding;

    // ArrayAdapter связывает объекты с элементами ListView
    public static RelativesArrayAdapter relativesArrayAdapter;
    public ListView relativesListView; // Для вывода информации

    public static FamiliesArrayAdapter familiesArrayAdapter;
    public ListView familiesListView;

    public static EditLocationsArrayAdapter editLocationsArrayAdapter;
    public ListView editLocationsListView;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        FamilyAndLocationsViewModel slideshowViewModel =
                new ViewModelProvider(this).get(FamilyAndLocationsViewModel.class);

        binding = FragmentFamilyAndLocationsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        TabLayout tabLayout = root.findViewById(R.id.tabFRL);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
           @Override
           public void onTabSelected(TabLayout.Tab tab) {
               FrameLayout linearLayoutFamily = root.findViewById(R.id.layoutListFamilies);
               FrameLayout linearLayoutRelatives = root.findViewById(R.id.layoutListRelatives);
               FrameLayout linearLayoutLocations = root.findViewById(R.id.layoutListLocations);
               switch (tab.getPosition()) {
                   case 0:
                       linearLayoutFamily.setVisibility(View.VISIBLE);
                       linearLayoutRelatives.setVisibility(View.GONE);
                       linearLayoutLocations.setVisibility(View.GONE);
                       break;
                   case 1:
                       linearLayoutFamily.setVisibility(View.GONE);
                       linearLayoutRelatives.setVisibility(View.VISIBLE);
                       linearLayoutLocations.setVisibility(View.GONE);
                       break;
                   case 2:
                       linearLayoutFamily.setVisibility(View.GONE);
                       linearLayoutRelatives.setVisibility(View.GONE);
                       linearLayoutLocations.setVisibility(View.VISIBLE);
                       break;
               }
           }

           @Override
           public void onTabUnselected(TabLayout.Tab tab) {

           }

           @Override
           public void onTabReselected(TabLayout.Tab tab) {

           }
        });
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        assert tab != null;
        tab.select();

        relativesListView = root.findViewById(R.id.listRelatives);
        relativesArrayAdapter = new RelativesArrayAdapter(getContext(), MainActivity.currentAccount.getRelatives());
        relativesListView.setAdapter(relativesArrayAdapter);
        TextView textView1 = root.findViewById(R.id.textViewIsNotRelative);
        textView1.setVisibility(MainActivity.currentAccount.getRelatives().size() > 0 ? View.GONE : View.VISIBLE);
        relativesArrayAdapter.registerDataSetObserver(new DataSetObserver()
        {
            @Override
            public void onChanged()
            {
                TextView textView = root.findViewById(R.id.textViewIsNotRelative);
                textView.setVisibility(MainActivity.currentAccount.getRelatives().size() > 0 ? View.GONE : View.VISIBLE);
            }
        });

        familiesListView = root.findViewById(R.id.listFamilies);
        familiesArrayAdapter = new FamiliesArrayAdapter(getContext(), MainActivity.currentAccount.getFamilies());
        familiesListView.setAdapter(familiesArrayAdapter);
        TextView textView = root.findViewById(R.id.textViewIsNotFamily);
        textView.setVisibility(MainActivity.currentAccount.getFamilies().size() > 0 ? View.GONE : View.VISIBLE);
        familiesArrayAdapter.registerDataSetObserver(new DataSetObserver()
        {
            @Override
            public void onChanged()
            {
                TextView textView = root.findViewById(R.id.textViewIsNotFamily);
                textView.setVisibility(MainActivity.currentAccount.getFamilies().size() > 0 ? View.GONE : View.VISIBLE);
            }
        });

        editLocationsListView = root.findViewById(R.id.listLocations);
        editLocationsArrayAdapter = new EditLocationsArrayAdapter(getContext(), MainActivity.currentAccount.getLocations());
        editLocationsListView.setAdapter(editLocationsArrayAdapter);
        TextView textView2 = root.findViewById(R.id.textViewIsNotLocation);
        textView2.setVisibility(MainActivity.currentAccount.getLocations().size() > 0 ? View.GONE : View.VISIBLE);
        editLocationsArrayAdapter.registerDataSetObserver(new DataSetObserver()
        {
            @Override
            public void onChanged()
            {
                TextView textView = root.findViewById(R.id.textViewIsNotLocation);
                textView.setVisibility(MainActivity.currentAccount.getLocations().size() > 0 ? View.GONE : View.VISIBLE);
            }
        });

        ImageButton btnCreateRelative = root.findViewById(R.id.btnAddRelative);
        btnCreateRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RelativeActivity.class);
                startActivity(intent);
            }
        });

        ImageButton btnCreateFamily = root.findViewById(R.id.btnAddFamily);
        btnCreateFamily.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FamilyActivity.class);
                startActivity(intent);
            }
        });

        ImageButton btnCreateLocation = root.findViewById(R.id.btnAddLocation);
        btnCreateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditLocationActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

/*    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        relativesArrayAdapter.notifyDataSetChanged();
        relativesListView.invalidateViews();
        relativesListView.refreshDrawableState();
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}