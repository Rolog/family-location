package com.morozov.familylocation.ui.location;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.PathOnMapActivity;
import com.morozov.familylocation.PositionOnMapActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.Utils.FamiliesArrayAdapter;
import com.morozov.familylocation.Utils.LocationFamilyArrayAdapter;
import com.morozov.familylocation.databinding.FragmentLocationBinding;
import com.morozov.familylocation.enteties.Position;
import com.morozov.familylocation.enteties.RelativeInLocation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class LocationFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    Spinner spinner;
    View root;

    public static LocationFamilyArrayAdapter locationFamilyArrayAdapter;
    public ListView locationFamilyListView;

    public List<RelativeInLocation> staticRelativeInLocations = new ArrayList<>();

    private FragmentLocationBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        LocationViewModel homeViewModel =
                new ViewModelProvider(this).get(LocationViewModel.class);

        binding = FragmentLocationBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        List<String> familyNames = new ArrayList<>();
        MainActivity.currentAccount.getFamilies().forEach(v -> {
            familyNames.add(v.getName());
        });
        spinner = root.findViewById(R.id.spinnerLocationSelectFamily);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(root.getContext(), android.R.layout.simple_list_item_1, familyNames.toArray(new String[0]));

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        locationFamilyListView = root.findViewById(R.id.lvLocationsFamily);
        locationFamilyArrayAdapter = new LocationFamilyArrayAdapter(getContext(), staticRelativeInLocations);
        locationFamilyListView.setAdapter(locationFamilyArrayAdapter);
        registerForContextMenu(locationFamilyListView);

        if (MainActivity.currentAccount.getId() != null)
            refreshLocationFamily();
    }

    public void refreshLocationFamily() {
        if (MainActivity.currentAccount.getFamilies().size() > 0)
            MainActivity.restRequestsService.getLocationMyFamily(MainActivity.currentAccount.getFamilies().get(spinner.getSelectedItemPosition()).getIdUUID());
    }

    public void drawLocationFamily(List<RelativeInLocation> relativeInLocations) {
        staticRelativeInLocations.clear();
        final List<RelativeInLocationCount>[] counts = new List[]{new ArrayList<>()};
        final Integer[] countOut = {0};
        MainActivity.currentAccount.getLocations().forEach(v -> {
            counts[0].add(new RelativeInLocationCount(v.getIdUUID(), v.getName(), 0));
        });
        relativeInLocations.forEach(v -> {
            if (v.getLocationId() != null)
                counts[0].stream().filter(f -> f.locationId.equals(v.getLocationId())).findFirst().orElse(new RelativeInLocationCount()).count += 1;
        });
        counts[0].sort((o1, o2) -> (o2.count.compareTo(o1.count)));
        counts[0].forEach(v -> {
            RelativeInLocation inLocation = new RelativeInLocation();
            inLocation.setLocationId(v.locationId);
            inLocation.setLocationName(v.locationName);
            inLocation.setModified(Long.valueOf(v.count));
            staticRelativeInLocations.add(inLocation);
            relativeInLocations.forEach(r -> {
                if (r.getLocationId() != null && r.getLocationId().equals(v.locationId)) {
                    staticRelativeInLocations.add(r);
                    countOut[0]++;
                }
            });
        });
        RelativeInLocation inLocation = new RelativeInLocation();
        UUID idOutsideLocation = UUID.randomUUID();
        String nameOutsideLocation = getString(R.string.outside_locations);
        inLocation.setLocationId(idOutsideLocation);
        inLocation.setLocationName(nameOutsideLocation);
        inLocation.setModified((long) (relativeInLocations.size() - countOut[0]));
        staticRelativeInLocations.add(inLocation);
        relativeInLocations.stream().filter(v -> v.getLocationId() == null).forEach(v -> {
            v.setLocationId(idOutsideLocation);
            v.setLocationName(nameOutsideLocation);
            staticRelativeInLocations.add(v);
        });
        locationFamilyArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        refreshLocationFamily();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId()==R.id.lvLocationsFamily) {
            MenuInflater inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.location_family_array_menu, menu);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        RelativeInLocation o = locationFamilyArrayAdapter.getItem(info.position);
        if (o.getRelativeId() == null)
            return super.onContextItemSelected(item);
        switch(item.getItemId()) {
            case R.id.toPositionOnMap:
                Intent intent = new Intent(getContext(), PositionOnMapActivity.class);
                intent.putExtra("lat", o.getLat());
                intent.putExtra("lon", o.getLon());
                intent.putExtra("name", o.getRelativeName());
                startActivity(intent);
                return true;
            case R.id.toPathOnMap:
                Intent intent1 = new Intent(getContext(), PathOnMapActivity.class);
                intent1.putExtra("id", o.getRelativeId().toString());
                intent1.putExtra("name", o.getRelativeName());
                Long time = System.currentTimeMillis();
                intent1.putExtra("start", time - 86400000);
                intent1.putExtra("end", time);
                startActivity(intent1);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }



    public static class RelativeInLocationCount {
        public UUID locationId;
        public String locationName;
        public Integer count;

        public RelativeInLocationCount() {
        }

        public RelativeInLocationCount(UUID locationId, String locationName, Integer count) {
            this.locationId = locationId;
            this.locationName = locationName;
            this.count = count;
        }
    }
}