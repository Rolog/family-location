package com.morozov.familylocation.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.RelativeActivity;
import com.morozov.familylocation.databinding.FragmentAccountBinding;

public class AccountFragment extends Fragment {

    private FragmentAccountBinding binding;
    private EditText etNewPassword;
    private EditText etNewPasswordReplay;
    Button btnChangePassword;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        AccountViewModel galleryViewModel =
                new ViewModelProvider(this).get(AccountViewModel.class);

        binding = FragmentAccountBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (etNewPassword.getText().length() > 0 && etNewPassword.getText().length() <= 5) {
                    etNewPassword.setError(root.getResources().getString(R.string.invalid_password));
                }
                if (etNewPasswordReplay.getText().length() > 0 && etNewPasswordReplay.getText().length() <= 5) {
                    etNewPasswordReplay.setError(root.getResources().getString(R.string.invalid_password));
                }
                btnChangePassword.setEnabled(etNewPassword.getText().toString().equals(etNewPasswordReplay.getText().toString()) &&
                        etNewPassword.getText().length() > 5 && etNewPasswordReplay.getText().length() > 5);
            }
        };
        etNewPassword = root.findViewById(R.id.editChangePassword);
        etNewPassword.addTextChangedListener(afterTextChangedListener);
        etNewPasswordReplay = root.findViewById(R.id.editChangePasswordReplay);
        etNewPasswordReplay.addTextChangedListener(afterTextChangedListener);

        btnChangePassword = root.findViewById(R.id.btnChangePassword);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.restRequestsService.changePasswordAccountToServer(MainActivity.currentAccount.getIdUUID(), etNewPassword.getText().toString());
                etNewPassword.setText("");
                etNewPasswordReplay.setText("");
            }
        });

        Button btnForceSync = root.findViewById(R.id.btnForcedSync);
        btnForceSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.restRequestsService.checkUpdateAccount();
                MainActivity.restRequestsService.updateRequestsme();
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}