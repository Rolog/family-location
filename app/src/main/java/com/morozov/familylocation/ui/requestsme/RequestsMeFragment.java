package com.morozov.familylocation.ui.requestsme;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.Utils.FamiliesArrayAdapter;
import com.morozov.familylocation.Utils.RelativesArrayAdapter;
import com.morozov.familylocation.Utils.RequestsmeArrayAdapter;
import com.morozov.familylocation.databinding.FragmentLocationBinding;
import com.morozov.familylocation.databinding.FragmentRequestsmeBinding;
import com.morozov.familylocation.enteties.Requestme;

import java.util.ArrayList;
import java.util.List;

public class RequestsMeFragment extends Fragment {

    private FragmentRequestsmeBinding binding;

    public static RequestsmeArrayAdapter requestsmeArrayAdapter;
    public ListView requestsmeListView;
    public static List<Requestme> requestmes = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        RequestsMeViewModel homeViewModel =
                new ViewModelProvider(this).get(RequestsMeViewModel.class);

        binding = FragmentRequestsmeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        setRequestMes();
        requestsmeListView = root.findViewById(R.id.listRequestsme);
        requestsmeArrayAdapter = new RequestsmeArrayAdapter(getContext(), requestmes);
        requestsmeListView.setAdapter(requestsmeArrayAdapter);
        return root;
    }

    public static void setRequestMes() {
        requestmes.clear();
        MainActivity.requestsmes.stream().filter(f -> f.getEmailRequest().equals(MainActivity.currentAccount.getEmail()))
                .forEach(v -> {
                    requestmes.add(v);
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}