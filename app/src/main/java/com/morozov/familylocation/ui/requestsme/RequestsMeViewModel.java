package com.morozov.familylocation.ui.requestsme;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RequestsMeViewModel extends ViewModel {

    private final MutableLiveData<String> mText;

    public RequestsMeViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}