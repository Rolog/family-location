package com.morozov.familylocation.Utils;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.morozov.familylocation.R;
import com.morozov.familylocation.enteties.Relative;

import java.util.List;

public class SelectRelativesArrayAdapter extends ArrayAdapter<Relative> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView id;
        TextView name;
        TextView email;
        CheckBox checkBox;
        LinearLayout linearLayout;
    }

    // Конструктор для инициализации унаследованных членов суперкласса
    public SelectRelativesArrayAdapter(Context context, List<Relative> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        Relative relative = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_select_relatives_item, parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.textIdSelectRelatives);
            viewHolder.name = (TextView) convertView.findViewById(R.id.textNameSelectRelatives);
            viewHolder.email = (TextView) convertView.findViewById(R.id.textEmailSelectRelatives);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBoxSelectRelatives);
            viewHolder.linearLayout = (LinearLayout) convertView.findViewById(R.id.llSelectRelatives);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        viewHolder.id.setText(relative.getId().toString());
        viewHolder.name.setText(relative.getName());
        viewHolder.email.setText(relative.getEmail());
        viewHolder.checkBox.setChecked(false);

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.checkBox.setChecked(!viewHolder.checkBox.isChecked());
            }
        });

        return convertView; // Вернуть готовое представление элемента
    }

}
