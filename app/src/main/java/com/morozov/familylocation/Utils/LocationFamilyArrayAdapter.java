package com.morozov.familylocation.Utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.RelativeActivity;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Relative;
import com.morozov.familylocation.enteties.RelativeInLocation;
import com.morozov.familylocation.ui.familyandlocations.FamilyAndLocationsFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class LocationFamilyArrayAdapter extends ArrayAdapter<RelativeInLocation> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView text;
        TextView time;
    }

    // Конструктор для инициализации унаследованных членов суперкласса
    public LocationFamilyArrayAdapter(Context context, List<RelativeInLocation> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        RelativeInLocation inLocation = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_location_family_item, parent, false);
            viewHolder.text = (TextView) convertView.findViewById(R.id.textLocationRelativeName);
            viewHolder.time = (TextView) convertView.findViewById(R.id.textTimeLocationRelativeName);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        if (inLocation.getRelativeId() != null) {
            viewHolder.text.setText(inLocation.getRelativeName());
            //viewHolder.text.setBackgroundResource(R.drawable.textview_relative_border);
            viewHolder.text.setBackgroundColor(context.getColor(R.color.transparent));
            viewHolder.text.setTypeface(ResourcesCompat.getFont(context, R.font.incendio));
            viewHolder.text.setTextColor(context.getColor(R.color.black));

            Date date = new Date(inLocation.getModified());
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy' 'HH:mm:ss");
            viewHolder.time.setText(simpleDateFormat.format(date));
            viewHolder.time.setTextColor(context.getColor(R.color.location_brown));
            viewHolder.time.setBackgroundColor(context.getColor(R.color.transparent));
        }
        else {
            viewHolder.text.setText(inLocation.getLocationName());
            //viewHolder.text.setBackgroundResource(R.drawable.textview_location_border);
            viewHolder.text.setBackgroundColor(context.getColor(R.color.location_brown));
            viewHolder.text.setTypeface(ResourcesCompat.getFont(context, R.font.harry_potter));
            viewHolder.text.setTextColor(context.getColor(R.color.white));
            viewHolder.time.setText("(" + inLocation.getModified().toString() + ")");
            viewHolder.time.setTextColor(context.getColor(R.color.white));
            viewHolder.time.setBackgroundColor(context.getColor(R.color.location_brown));

        }

        return convertView; // Вернуть готовое представление элемента
    }

}
