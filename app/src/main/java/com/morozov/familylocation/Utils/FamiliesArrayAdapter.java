package com.morozov.familylocation.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.morozov.familylocation.FamilyActivity;
import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Family;
import com.morozov.familylocation.ui.familyandlocations.FamilyAndLocationsFragment;

import java.util.List;

public class FamiliesArrayAdapter extends ArrayAdapter<Family> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView id;
        TextView name;
        ImageButton edit;
        ImageButton remove;
    }

    // Конструктор для инициализации унаследованных членов суперкласса
    public FamiliesArrayAdapter(Context context, List<Family> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        Family family = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_family_item, parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.textIdFamily);
            viewHolder.name = (TextView) convertView.findViewById(R.id.textNameFamily);
            viewHolder.edit = (ImageButton) convertView.findViewById(R.id.btnEditFamily);
            viewHolder.remove = (ImageButton) convertView.findViewById(R.id.btnRemoveFamily);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        viewHolder.id.setText(family.getId().toString());
        viewHolder.name.setText(family.getName());

        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(context);

                builder.setTitle(getContext().getString(R.string.are_you_sure));
                builder.setMessage(Html.fromHtml(getContext().getString(R.string.family_will_be_removed).replace("{?}", viewHolder.name.getText()), Html.FROM_HTML_MODE_LEGACY));

                // provide an OK button that simply dismisses the dialog
                builder.setPositiveButton(getContext().getString(R.string.delete),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog, int button) {
                                getContext().getContentResolver().delete(DatabaseDescription.DBFamilies.buildContactUri(family.getId()), null, null);
                                MainActivity.dataBaseEntityService.loadCurrentAccount();
                                FamilyAndLocationsFragment.familiesArrayAdapter.notifyDataSetChanged();
                            }
                        }
                );

                builder.setNegativeButton(getContext().getString(R.string.cancel), null);
                builder.create(); // return the AlertDialog
                builder.show();
            }
        });

        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), FamilyActivity.class);
                intent.putExtra("id", family.getId());
                getContext().startActivity(intent);
            }
        });


        return convertView; // Вернуть готовое представление элемента
    }

}
