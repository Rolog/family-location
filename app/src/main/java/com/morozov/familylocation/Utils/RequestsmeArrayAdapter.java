package com.morozov.familylocation.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Requestme;
import com.morozov.familylocation.ui.requestsme.RequestsMeFragment;

import java.util.List;

public class RequestsmeArrayAdapter extends ArrayAdapter<Requestme> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView id;
        TextView idWho;
        TextView emailWho;
        TextView emailRequest;
        Switch permission;
    }

    // Конструктор для инициализации унаследованных членов суперкласса
    public RequestsmeArrayAdapter(Context context, List<Requestme> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        Requestme requestsme = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_requestsme_item, parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.textIdRequestsme);
            viewHolder.idWho = (TextView) convertView.findViewById(R.id.textIdWhoRequestsme);
            viewHolder.emailWho = (TextView) convertView.findViewById(R.id.textEmailWhoRequestsme);
            viewHolder.emailRequest = (TextView) convertView.findViewById(R.id.textEmailRequestRequestsme);
            viewHolder.permission = (Switch) convertView.findViewById(R.id.btnPermissionRelativeRequestsme);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        viewHolder.id.setText(requestsme.getId().toString());
        viewHolder.idWho.setText(requestsme.getIdWho().toString());
        viewHolder.emailWho.setText(requestsme.getEmailWho());
        viewHolder.emailRequest.setText(requestsme.getEmailRequest());
        viewHolder.permission.setChecked(requestsme.isPermission());

        viewHolder.permission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(DatabaseDescription.DBRequestsme.COLUMN_PERMISSION, viewHolder.permission.isChecked());
                values.put(DatabaseDescription.DBRequestsme.COLUMN_MODIFIED, System.currentTimeMillis());
                MainActivity.thisStatic.getContentResolver().update(
                        DatabaseDescription.DBRequestsme.buildContactUri(requestsme.getId()), values, null, null);
                MainActivity.restRequestsService.exportRequestmeToServer(MainActivity.dataBaseEntityService.getRequestsMe(requestsme.getId()));
                MainActivity.dataBaseEntityService.loadRequestsMe();
                RequestsMeFragment.setRequestMes();
                RequestsMeFragment.requestsmeArrayAdapter.notifyDataSetChanged();
            }
        });

        return convertView; // Вернуть готовое представление элемента
    }

}
