package com.morozov.familylocation.Utils;

public class Constants {
    public static Integer RESULT_OK = 1;
    public static final int PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 123;
    public static final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 124;
    public static final int RESULT_SETTINGS_OK = 125;
}
