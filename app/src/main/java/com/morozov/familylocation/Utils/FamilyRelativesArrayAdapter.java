package com.morozov.familylocation.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.morozov.familylocation.R;
import com.morozov.familylocation.enteties.Relative;

import java.util.List;

public class FamilyRelativesArrayAdapter extends ArrayAdapter<Relative> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView id;
        TextView name;
        TextView email;
        ImageButton permission;
        ImageButton edit;
        ImageButton remove;
    }

    // Конструктор для инициализации унаследованных членов суперкласса
    public FamilyRelativesArrayAdapter(Context context, List<Relative> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        Relative relative = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_relatives_item, parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.textId);
            viewHolder.name = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.email = (TextView) convertView.findViewById(R.id.textEmail);
            viewHolder.permission = (ImageButton) convertView.findViewById(R.id.btnPermissionRelative);
            viewHolder.edit = (ImageButton) convertView.findViewById(R.id.btnEditRelative);
            viewHolder.remove = (ImageButton) convertView.findViewById(R.id.btnRemoveRelative);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        viewHolder.id.setText(relative.getId().toString());
        viewHolder.name.setText(relative.getName());
        viewHolder.email.setText(relative.getEmail());
        viewHolder.permission.setVisibility(View.GONE);
        viewHolder.edit.setVisibility(View.GONE);
        float density = context.getResources().getDisplayMetrics().density;
       /* viewHolder.remove.setLayoutParams(new LinearLayout.LayoutParams((int)(context.getResources().getDimension(R.dimen.remove_button_width) * density),
                (int)(context.getResources().getDimension(R.dimen.remove_button_width) * density)));*/

        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(context);

                builder.setTitle(getContext().getString(R.string.are_you_sure));
                builder.setMessage(Html.fromHtml(getContext().getString(R.string.relative_will_be_removed_from_family).replace("{?}", viewHolder.email.getText()), Html.FROM_HTML_MODE_LEGACY));

                // provide an OK button that simply dismisses the dialog
                builder.setPositiveButton(getContext().getString(R.string.delete),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(
                                    DialogInterface dialog, int button) {
                                remove(relative);
                                notifyDataSetChanged();
                            }
                        }
                );

                builder.setNegativeButton(getContext().getString(R.string.cancel), null);
                builder.create(); // return the AlertDialog
                builder.show();
            }
        });

        return convertView; // Вернуть готовое представление элемента
    }

}
