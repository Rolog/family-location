package com.morozov.familylocation.Utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;
import com.morozov.familylocation.RelativeActivity;
import com.morozov.familylocation.data.DatabaseDescription;
import com.morozov.familylocation.enteties.Relative;
import com.morozov.familylocation.ui.familyandlocations.FamilyAndLocationsFragment;

import java.util.List;

public class RelativesArrayAdapter extends ArrayAdapter<Relative> {
    // Класс для повторного использования представлений списка при прокрутке
    private static class ViewHolder {
        TextView id;
        TextView name;
        TextView email;
        ImageButton permission;
        ImageButton edit;
        ImageButton remove;
    }

    // Конструктор для инициализации унаследованных членов суперкласса
    public RelativesArrayAdapter(Context context, List<Relative> forecast) {
        super(context, -1, forecast);
    }

    // Создание пользовательских представлений для элементов ListView
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Получение объекта Weather для заданной позиции ListView
        Relative relative = getItem(position);

        ViewHolder viewHolder; // Объект, содержащий ссылки
        // на представления элемента списка
        // Проверить возможность повторного использования ViewHolder
        // для элемента, вышедшего за границы экрана
        if (convertView == null) { // Объекта ViewHolder нет, создать его
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_relatives_item, parent, false);
            viewHolder.id = (TextView) convertView.findViewById(R.id.textId);
            viewHolder.name = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.email = (TextView) convertView.findViewById(R.id.textEmail);
            viewHolder.permission = (ImageButton) convertView.findViewById(R.id.btnPermissionRelative);
            viewHolder.edit = (ImageButton) convertView.findViewById(R.id.btnEditRelative);
            viewHolder.remove = (ImageButton) convertView.findViewById(R.id.btnRemoveRelative);
            convertView.setTag(viewHolder);
        }
        else { // Cуществующий объект ViewHolder используется заново
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Получить данные из объекта Weather и заполнить представления
        Context context = getContext(); // Для загрузки строковых ресурсов
        viewHolder.id.setText(relative.getId().toString());
        viewHolder.name.setText(relative.getName());
        viewHolder.email.setText(relative.getEmail());

        if (relative.isPermission()) {
            viewHolder.permission.setImageResource(R.drawable.ic_user_check_solid);
            viewHolder.permission.setColorFilter(context.getColor(R.color.green));
        } else {
            viewHolder.permission.setImageResource(R.drawable.ic_user_xmark_solid);
            viewHolder.permission.setColorFilter(context.getColor(R.color.red));
        }

        viewHolder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.app.AlertDialog.Builder builder =
                        new AlertDialog.Builder(context);

                builder.setTitle(getContext().getString(R.string.are_you_sure));
                builder.setMessage(Html.fromHtml(getContext().getString(R.string.relative_will_be_removed).replace("{?}", viewHolder.email.getText()), Html.FROM_HTML_MODE_LEGACY));

                // provide an OK button that simply dismisses the dialog
                builder.setPositiveButton(getContext().getString(R.string.delete),
                        new DialogInterface.OnClickListener() {
                            @SuppressLint("Range")
                            @Override
                            public void onClick(
                                    DialogInterface dialog, int button) {
                                getContext().getContentResolver().delete(DatabaseDescription.DBRelatives.buildContactUri(relative.getId()), null, null);
                                int CURSOR_ID;
                                Cursor cursor = MainActivity.thisStatic.getContentResolver().query(
                                        DatabaseDescription.DBFamiliesRelatives.CONTENT_URI,
                                        null,
                                        DatabaseDescription.DBFamiliesRelatives.COLUMN_ID_RELATIVE + "='" + relative.getId() + "'", null, null);
                                while (cursor.moveToNext()) {
                                    CURSOR_ID = cursor.getInt(cursor.getColumnIndex(DatabaseDescription.DBFamiliesRelatives._ID));
                                    MainActivity.thisStatic.getContentResolver().delete(DatabaseDescription.DBFamiliesRelatives.buildContactUri(CURSOR_ID), null, null);
                                }
                                cursor.close();
                                MainActivity.dataBaseEntityService.loadCurrentAccount();
                                FamilyAndLocationsFragment.relativesArrayAdapter.notifyDataSetChanged();
                            }
                        }
                );

                builder.setNegativeButton(getContext().getString(R.string.cancel), null);
                builder.create(); // return the AlertDialog
                builder.show();
            }
        });

        viewHolder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), RelativeActivity.class);
                intent.putExtra("id", relative.getId());
                getContext().startActivity(intent);
            }
        });


        return convertView; // Вернуть готовое представление элемента
    }

}
