package com.morozov.familylocation.update;

import androidx.appcompat.app.AlertDialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.morozov.familylocation.AsyncTask.RestRequestsService;
import com.morozov.familylocation.MainActivity;
import com.morozov.familylocation.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class CheckForUpdate extends AsyncTask<Void, Void, Void> implements DialogInterface.OnCancelListener {
    private static final String LOG_TAG             = CheckForUpdate.class.getName();

    private static DownloadAndInstallUpdate downloadAndInstallUpdate;

    private Context context;
    private AlertDialog progressDialog;
    private AlertDialog dialog;
    private boolean isRun;
    private UpdateInfo info;
    private final boolean showSearchUpdateMessage;
    private final boolean showNoUpdateMessage;

    public CheckForUpdate(Context context, boolean showSearchUpdateMessage, boolean showNoUpdateMessage) {
        this.context = context;
        this.showSearchUpdateMessage = showSearchUpdateMessage;
        this.showNoUpdateMessage = showNoUpdateMessage;
        isRun = false;
    }

    private void initProgressDialog() {
        progressDialog = Dialogs.circularProgressDialog(context,
                context.getString(R.string.update_title), context.getString(R.string.update_search),
                true, this).getAlertDialog();
    }

    public void onCreate(Context context) {
        this.context = context;
        if (downloadAndInstallUpdate != null) {
            downloadAndInstallUpdate.onCreate(context);
        }
        initProgressDialog();
        if (isRun && showSearchUpdateMessage) {
            progressDialog.show();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        initProgressDialog();
        if (showSearchUpdateMessage)
            progressDialog.show();
        isRun = true;
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (progressDialog != null && progressDialog.isShowing() && !(((AppCompatActivity)context).isFinishing()))
            progressDialog.dismiss();
        isRun = false;
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(context);
        if (info == null) {
            builder.setTitle(R.string.update_title);
            builder.setMessage(R.string.update_network_server_error);
            builder.setPositiveButton(R.string.ok, null);
        } else {
            try {
                if (info.getVersionCode() > context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode) {
                    builder.setTitle(R.string.update_title);
                    builder.setMessage(context.getString(R.string.update_exists, info.getVersionName()));
                    builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            downloadAndInstallUpdate = new DownloadAndInstallUpdate(context, info);
                            downloadAndInstallUpdate.execute();
                        }
                    });
                    builder.setNeutralButton(R.string.no, null);
                } else {
                    if (showNoUpdateMessage) {
                        builder.setTitle(R.string.update_title);
                        builder.setMessage(R.string.update_not_found);
                        builder.setPositiveButton(R.string.ok, null);
                    } else {
                        return;
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        dialog = builder.show();
    }

    public void dismiss() {
        if (progressDialog != null && progressDialog.isShowing() && !(((AppCompatActivity)context).isFinishing()))
            progressDialog.dismiss();
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    protected void onCancelled(Void aVoid) {
        isRun = false;
        super.onCancelled(aVoid);
        if (progressDialog != null && progressDialog.isShowing() && !(((AppCompatActivity)context).isFinishing()))
            progressDialog.dismiss();
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        cancel(true);
        isRun = false;
        if (progressDialog != null && progressDialog.isShowing() && !(((AppCompatActivity)context).isFinishing()))
            progressDialog.dismiss();
        if (this.dialog != null && this.dialog.isShowing())
            this.dialog.dismiss();
        if (downloadAndInstallUpdate != null)
            downloadAndInstallUpdate.cancel(true);
    }

    @Override
    protected Void doInBackground(Void... params) {
        int count;
        try {
            URL url = new URL("https://bitbucket.org/Rolog/family-location/downloads/version.json");

            URLConnection conection = url.openConnection();
            conection.connect();

            // input stream to read file - with 8k buffer
            InputStream input = new BufferedInputStream(url.openStream());

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            byte[] data = new byte[16384];

            while ((count = input.read(data)) != -1) {
                buffer.write(data, 0, count);
            }

            info = MainActivity.restRequestsService.JSONtoClass(buffer.toString().replace("\n", ""), UpdateInfo.class);
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }
        return null;
    }
}
